package com.aleksandrp.lightaquarium.shar;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import com.aleksandrp.lightaquarium.db.TimePointModel;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.jjoe64.graphview.series.Series;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

public class CharUtils {

    private static ArrayList<TimePointModel> pointModels = new ArrayList<>();

    private static ClickPointListener pointListener;

    private static GraphView graph;
    private static LineGraphSeries<DataPoint> lineGraphSeries;
    private static Activity context;
    private static PointsGraphSeries<DataPoint> tapDataPoints = new PointsGraphSeries<>();
    private static LineGraphSeries<DataPoint> verticalLineDataPoints = new LineGraphSeries<>();


    public static void initChars(Activity context, GraphView chart_specktr) {
        CharUtils.context = context;
        CharUtils.graph = chart_specktr;
        lineGraphSeries = new LineGraphSeries<>();
        setGraph();
        setSeries();

        setData(new ArrayList<>());
    }

    private static void setGraph() {
        // activate horizontal zooming and scrolling
        graph.getViewport().setScalable(false);
        // activate horizontal scrolling
        graph.getViewport().setScrollable(false);
        // activate horizontal and vertical zooming and scrolling
        graph.getViewport().setScalableY(false);
        // activate vertical scrolling
        graph.getViewport().setScrollableY(false);
        // set manual X bounds
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(getDateDouble("00:00"));
        graph.getViewport().setMaxX(getDateDouble("24:00"));
        // set manual Y bounds
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0.0);
        graph.getViewport().setMaxY(100.0);
        // custom label formatter to show currency "EUR"
        graph.getGridLabelRenderer().setHighlightZeroLines(true);
        graph.getGridLabelRenderer().setGridColor(Color.WHITE);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setVerticalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value_, boolean isValueX) {
                String s = super.formatLabel(value_, isValueX);

                if (isValueX) {
                    Log.d("DATE_TEST", "value_ " + value_);
                    SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    String t_HH_mm_ss = format.format(value_);
                    return t_HH_mm_ss;
                } else {
                    // show currency for y values
                    return s;
                }
            }
        });

        //One important point: When using dates as X values, the rounding of the
        // bounds to nice human-readable numbers does not make sense. So you should
        // disable this feature via the method graph.getGridLabelRenderer().setHumanRounding(false);
        graph.getGridLabelRenderer().setHumanRounding(false);
    }

    private static void setSeries() {
        lineGraphSeries.setOnDataPointTapListener(listener);

        lineGraphSeries.setDrawDataPoints(true);
        lineGraphSeries.setAnimated(true);
        lineGraphSeries.setDrawBackground(true);
        lineGraphSeries.setBackgroundColor(Color.parseColor("#260C1441"));
        lineGraphSeries.setColor(Color.YELLOW);
        lineGraphSeries.setDataPointsRadius(10f);

// custom paint to make a dotted line
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5f);
        paint.setStrokeMiter(50);
//        paint.setColor(Color.YELLOW);
        paint.setColor(Color.parseColor("#FFFFC107"));
        lineGraphSeries.setCustomPaint(paint);
    }

//    ==============================================================================================

    public static void showPointChar(TimePointModel model) {
        setTapShow(getDateDouble(model.getTime()), model.getPort_value_general());
    }

    public static void setData(ArrayList<TimePointModel> pointModels_) {
        CharUtils.pointModels = pointModels_;
        if (pointModels.size() > 0) {
//            TimePointModel max = Collections.max(pointModels, new Comparator<TimePointModel>() {
//                @Override
//                public int compare(TimePointModel first, TimePointModel second) {
//                    if (first.getPort_value_general() > second.getPort_value_general())
//                        return 1;
//                    else if (first.getPort_value_general() < second.getPort_value_general())
//                        return -1;
//                    return 0;
//                }
//            });

            graph.removeAllSeries();
            lineGraphSeries.resetData(new DataPoint[0]);

            for (int i = 0; i < pointModels.size(); i++) {
                TimePointModel model = pointModels.get(i);
                int port_value_general = model.getPort_value_general();
                if (port_value_general > 127) {
                    port_value_general = port_value_general - 128;
                    port_value_general = port_value_general / 10;
                }
                DataPoint point = new DataPoint(getDateDouble(model.getTime()), port_value_general);
                lineGraphSeries.appendData(point, false, pointModels.size(), true);
            }
            graph.addSeries(lineGraphSeries);
        } else {
//            yMax = 100.0;
            graph.removeAllSeries();
            lineGraphSeries.resetData(new DataPoint[0]);
        }
    }

    public static double getDateDouble(String time) {
        Date date = null;
        try {
            date = new SimpleDateFormat("HH:mm", Locale.getDefault()).parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
        return date.getTime();
    }

    static OnDataPointTapListener listener = new OnDataPointTapListener() {
        @Override
        public void onTap(Series series_, DataPointInterface dataPoint) {
            context.runOnUiThread(() -> {
                graph.postDelayed(() -> {
                    setTapShow(dataPoint.getX(), dataPoint.getY());
                    if (pointListener != null) {
                        pointListener.selectedPoint(dataPoint.getX());
                    }
                }, 100);
            });
        }
    };

    private static void setTapShow(double x, double y) {
        if (y >= 128) {
            y = (y - 128) / 10;
        }
        try {
            graph.removeSeries(tapDataPoints);
            tapDataPoints = new PointsGraphSeries<>();
        } catch (Exception e) {
            e.printStackTrace();
        }

        DataPoint point = new DataPoint(x, y);
        tapDataPoints.appendData(point, false, pointModels.size(), true);
        graph.addSeries(tapDataPoints);
        tapDataPoints.setShape(PointsGraphSeries.Shape.POINT);
        tapDataPoints.setColor(Color.RED);
        tapDataPoints.setSize(12f);
    }

    public static void setPointListener(ClickPointListener pointListener) {
        CharUtils.pointListener = pointListener;
    }

    public static void showVerticalLineTimeChar(String time) {
        double date = getDateDouble(time);
        DataPoint point1 = new DataPoint(date, 0);
        DataPoint point2 = new DataPoint(date, 100);
        try {
            graph.removeSeries(verticalLineDataPoints);
            verticalLineDataPoints = new LineGraphSeries<>();
        } catch (Exception e) {
            e.printStackTrace();
        }

        verticalLineDataPoints.appendData(point1, false, 2, true);
        verticalLineDataPoints.appendData(point2, false, 2, true);
        graph.addSeries(verticalLineDataPoints);
        verticalLineDataPoints.setColor(Color.RED);
        verticalLineDataPoints.setDrawDataPoints(false);
        verticalLineDataPoints.setAnimated(false);
        verticalLineDataPoints.setDrawBackground(false);
        verticalLineDataPoints.setThickness(1);
    }

    public interface ClickPointListener {
        void selectedPoint(double x);
    }
}

package com.aleksandrp.lightaquarium.db;

import java.util.ArrayList;

public interface DbHelper {

    void saveController(
            int identificator, int countData, int count_pointsd,
            int data_1, int data_2, int data_3, int data_4, int data_5, int data_6,
            int data_7, int data_8, int data_9, int data_10, int data_11, int data_12,
            int data_13, int data_14, int data_15);

    void savePoints(
            int identificator, int countData, int count_pointsd,
            int data_1, int data_2, int data_3, int data_4, int data_5, int data_6,
            int data_7, int data_8, int data_9, int data_10, int data_11, int data_12,
            int data_13, int data_14, int data_15, DbHelperImpl.SaveListener saveListener);

    ArrayList<TimePointModel> readAllPoints();

    void removePoint(TimePointModel currentPoint);

    void removeAllPoints();

    void addNewPoint(int time_HH, int time_MM, long maxId, TimePointModel model);

    void editTimePointModel(TimePointModel model);
}

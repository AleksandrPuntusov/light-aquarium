package com.aleksandrp.lightaquarium.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ControllerModel extends RealmObject {

    @PrimaryKey
    private long id;
    private String name;
    private String model;
    private int canals;
    private long curent_time;
    private int points;

    public ControllerModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCanals() {
        return canals;
    }

    public void setCanals(int canals) {
        this.canals = canals;
    }

    public long getCurent_time() {
        return curent_time;
    }

    public void setCurent_time(long curent_time) {
        this.curent_time = curent_time;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}

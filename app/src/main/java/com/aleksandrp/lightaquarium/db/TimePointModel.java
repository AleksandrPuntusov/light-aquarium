package com.aleksandrp.lightaquarium.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TimePointModel extends RealmObject {

    @PrimaryKey
    private long id;
    private int canals;
    private boolean enable_pomp;
    private boolean is_night_1;
    private boolean is_night_2;
    private boolean is_night_3;
    private boolean is_night_4;
    private boolean is_night_5;
    private boolean is_night_6;
    private boolean is_night_7;
    private boolean is_night_8;
    private boolean is_night_9;
    private boolean is_night_10;
    private boolean is_night_11;
    private boolean is_night_12;
    private boolean is_edit;
    private String name;
    private String power;
    private String time;
    private String color;
    private String temperature;
    // value sum in canals
    private int port_value_general;
    // value in color canal point
    private String port_value_1;
    private String port_value_2;
    private String port_value_3;
    private String port_value_4;
    private String port_value_5;
    private String port_value_6;
    private String port_value_7;
    private String port_value_8;
    private String port_value_9;
    private String port_value_10;
    private String port_value_11;
    private String port_value_12;

    public TimePointModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCanals() {
        return canals;
    }

    public void setCanals(int canals) {
        this.canals = canals;
    }

    public boolean isEnable_pomp() {
        return enable_pomp;
    }

    public void setEnable_pomp(boolean enable_pomp) {
        this.enable_pomp = enable_pomp;
    }

    public boolean isIs_night_1() {
        return is_night_1;
    }

    public void setIs_night_1(boolean is_night_1) {
        this.is_night_1 = is_night_1;
    }

    public boolean isIs_night_2() {
        return is_night_2;
    }

    public void setIs_night_2(boolean is_night_2) {
        this.is_night_2 = is_night_2;
    }

    public boolean isIs_night_3() {
        return is_night_3;
    }

    public void setIs_night_3(boolean is_night_3) {
        this.is_night_3 = is_night_3;
    }

    public boolean isIs_night_4() {
        return is_night_4;
    }

    public void setIs_night_4(boolean is_night_4) {
        this.is_night_4 = is_night_4;
    }

    public boolean isIs_night_5() {
        return is_night_5;
    }

    public void setIs_night_5(boolean is_night_5) {
        this.is_night_5 = is_night_5;
    }

    public boolean isIs_night_6() {
        return is_night_6;
    }

    public void setIs_night_6(boolean is_night_6) {
        this.is_night_6 = is_night_6;
    }

    public boolean isIs_night_7() {
        return is_night_7;
    }

    public void setIs_night_7(boolean is_night_7) {
        this.is_night_7 = is_night_7;
    }

    public boolean isIs_night_8() {
        return is_night_8;
    }

    public void setIs_night_8(boolean is_night_8) {
        this.is_night_8 = is_night_8;
    }

    public boolean isIs_night_9() {
        return is_night_9;
    }

    public void setIs_night_9(boolean is_night_9) {
        this.is_night_9 = is_night_9;
    }

    public boolean isIs_night_10() {
        return is_night_10;
    }

    public void setIs_night_10(boolean is_night_10) {
        this.is_night_10 = is_night_10;
    }

    public boolean isIs_night_11() {
        return is_night_11;
    }

    public void setIs_night_11(boolean is_night_11) {
        this.is_night_11 = is_night_11;
    }

    public boolean isIs_night_12() {
        return is_night_12;
    }

    public void setIs_night_12(boolean is_night_12) {
        this.is_night_12 = is_night_12;
    }

    public boolean isIs_edit() {
        return is_edit;
    }

    public void setIs_edit(boolean is_edit) {
        this.is_edit = is_edit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public int getPort_value_general() {
        return port_value_general;
    }

    public void setPort_value_general(int port_value_general) {
        this.port_value_general = port_value_general;
    }

    public String getPort_value_1() {
        return port_value_1;
    }

    public void setPort_value_1(String port_value_1) {
        this.port_value_1 = port_value_1;
    }

    public String getPort_value_2() {
        return port_value_2;
    }

    public void setPort_value_2(String port_value_2) {
        this.port_value_2 = port_value_2;
    }

    public String getPort_value_3() {
        return port_value_3;
    }

    public void setPort_value_3(String port_value_3) {
        this.port_value_3 = port_value_3;
    }

    public String getPort_value_4() {
        return port_value_4;
    }

    public void setPort_value_4(String port_value_4) {
        this.port_value_4 = port_value_4;
    }

    public String getPort_value_5() {
        return port_value_5;
    }

    public void setPort_value_5(String port_value_5) {
        this.port_value_5 = port_value_5;
    }

    public String getPort_value_6() {
        return port_value_6;
    }

    public void setPort_value_6(String port_value_6) {
        this.port_value_6 = port_value_6;
    }

    public String getPort_value_7() {
        return port_value_7;
    }

    public void setPort_value_7(String port_value_7) {
        this.port_value_7 = port_value_7;
    }

    public String getPort_value_8() {
        return port_value_8;
    }

    public void setPort_value_8(String port_value_8) {
        this.port_value_8 = port_value_8;
    }

    public String getPort_value_9() {
        return port_value_9;
    }

    public void setPort_value_9(String port_value_9) {
        this.port_value_9 = port_value_9;
    }

    public String getPort_value_10() {
        return port_value_10;
    }

    public void setPort_value_10(String port_value_10) {
        this.port_value_10 = port_value_10;
    }

    public String getPort_value_11() {
        return port_value_11;
    }

    public void setPort_value_11(String port_value_11) {
        this.port_value_11 = port_value_11;
    }

    public String getPort_value_12() {
        return port_value_12;
    }

    public void setPort_value_12(String port_value_12) {
        this.port_value_12 = port_value_12;
    }
}

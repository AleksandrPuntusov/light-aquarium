package com.aleksandrp.lightaquarium.db;

import android.util.Log;

import com.aleksandrp.lightaquarium.utils.SettingsApp;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class DbHelperImpl implements DbHelper {

    public static final String TAG = "DB_TAG";
    private static DbHelperImpl dbHelper;

    private DbHelperImpl() {
    }

    public static DbHelperImpl getInstance() {
        if (dbHelper == null) {
            dbHelper = new DbHelperImpl();
        }
        return dbHelper;
    }


    @Override
    public void saveController(
            int identificator, int countData, int count_canals,
            int time_HH, int time_MM, int time_SS, int count_points, int data_5, int data_6,
            int data_7, int data_8, int data_9, int data_10, int data_11, int data_12,
            int data_13, int data_14, int data_15) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(
                realm1 -> {
                    ControllerModel controllerModel = realm.where(ControllerModel.class)
                            .equalTo("id", 1)
                            .findFirst();
                    if (controllerModel == null) {
                        controllerModel = realm.createObject(ControllerModel.class, 1);
                    }
                    controllerModel.setCanals(count_canals);
                    controllerModel.setPoints(count_points);
//                }, error -> {
//                    Log.d(TAG, "Error saveController " + error.getMessage());
                });
    }

    public interface SaveListener {
        void isSave(boolean save);
    }


    @Override
    public void savePoints(
            int identificator, int countData, int count_pointsd,
            int time_HH, int time_MM, int data_1, int data_2, int data_3, int data_4,
            int data_5, int data_6, int data_7, int data_8, int data_9, int data_10,
            int data_11, int data_12, int data_13, SaveListener saveListener) {

        TimePointModel pointModel = new TimePointModel();
        pointModel.setId(identificator);
        pointModel.setTime(refactorTime(time_HH, time_MM));

        if (data_1 > 127) {
            pointModel.setIs_night_1(true);
        }
        pointModel.setPort_value_1((String.valueOf(data_1)));

        if (data_2 > 127) {
            pointModel.setIs_night_2(true);
        }
        pointModel.setPort_value_2((String.valueOf(data_2)));

        if (data_3 > 127) {
            pointModel.setIs_night_3(true);
        }
        pointModel.setPort_value_3((String.valueOf(data_3)));

        if (data_4 > 127) {
            pointModel.setIs_night_4(true);
        }
        pointModel.setPort_value_4((String.valueOf(data_4)));
        if (data_5 > 127) {
            pointModel.setIs_night_5(true);
        }
        pointModel.setPort_value_5((String.valueOf(data_5)));

        if (data_6 > 127) {
            pointModel.setIs_night_6(true);
        }
        pointModel.setPort_value_6((String.valueOf(data_6)));

        if (data_7 > 127) {
            pointModel.setIs_night_7(true);
        }
        pointModel.setPort_value_7((String.valueOf(data_7)));

        if (data_8 > 127) {
            pointModel.setIs_night_8(true);
        }
        pointModel.setPort_value_8((String.valueOf(data_8)));

        if (data_9 > 127) {
            pointModel.setIs_night_9(true);
        }
        pointModel.setPort_value_9((String.valueOf(data_9)));

        if (data_10 > 127) {
            pointModel.setIs_night_10(true);
        }
        pointModel.setPort_value_10((String.valueOf(data_10)));

        if (data_11 > 127) {
            pointModel.setIs_night_11(true);
        }
        pointModel.setPort_value_11((String.valueOf(data_11)));

        if (data_12 > 127) {
            pointModel.setIs_night_12(true);
        }
        pointModel.setPort_value_12((String.valueOf(data_12)));

        int sum = getSum(data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8, data_9, data_10, data_11, data_12);
        pointModel.setPort_value_general(sum);

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(
                realm1 -> {
                    realm1.copyToRealmOrUpdate(pointModel);
                },
                () -> {
                    if (saveListener != null) {
                        saveListener.isSave(true);
                    }
                    Log.d("CREATE_UPDATE_DB", "CREATE true");
                },
                error -> {
                    if (saveListener != null) {
                        saveListener.isSave(false);
                    }
                    Log.d("CREATE_ERROR_DB", error.getMessage());
                });
    }


    @Override
    public ArrayList<TimePointModel> readAllPoints() {
        Realm realm = Realm.getDefaultInstance();
        List<TimePointModel> timePointModels =
                realm.copyFromRealm(realm.where(TimePointModel.class)
//                        .notEqualTo("port_value_general", "00")
//                        .and()
//                        .notEqualTo("port_value_general", "0")
                        .sort("time", Sort.ASCENDING)
                        .findAll());
        return new ArrayList<>(timePointModels);
    }

    @Override
    public void removePoint(TimePointModel currentPoint) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                long id = currentPoint.getId();
                RealmResults<TimePointModel> timePointModels = realm.where(TimePointModel.class)
                        .equalTo("id", id)
                        .findAll();
                timePointModels.deleteAllFromRealm();
            }
        });
    }

    @Override
    public void removeAllPoints() {
        Realm realm1 = Realm.getDefaultInstance();
        realm1.executeTransaction(
                realm -> {
                    try {
                        realm.delete(TimePointModel.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    @Override
    public void addNewPoint(int time_HH, int time_MM, long maxId, TimePointModel model) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(
                realm1 -> {
                    TimePointModel pointModel = realm.where(TimePointModel.class)
                            .equalTo("id", maxId)
                            .findFirst();
                    if (pointModel == null) {
                        pointModel = realm.createObject(TimePointModel.class, maxId);
                    }
                    pointModel.setTime(refactorTime(time_HH, time_MM));
                    pointModel.setPort_value_1(model.getPort_value_1());
                    pointModel.setPort_value_2(model.getPort_value_2());
                    pointModel.setPort_value_3(model.getPort_value_3());
                    pointModel.setPort_value_4(model.getPort_value_4());
                    pointModel.setPort_value_5(model.getPort_value_5());
                    pointModel.setPort_value_6(model.getPort_value_6());
                    pointModel.setPort_value_7(model.getPort_value_7());
                    pointModel.setPort_value_8(model.getPort_value_8());
                    pointModel.setPort_value_9(model.getPort_value_9());
                    pointModel.setPort_value_10(model.getPort_value_10());
                    pointModel.setPort_value_11(model.getPort_value_11());
                    pointModel.setPort_value_12(model.getPort_value_12());
                    pointModel.setPort_value_general(model.getPort_value_general());

//                }, error -> {
//                    Log.d(TAG, "Error savePoints " + error.getMessage());
                });
    }

    @Override
    public void editTimePointModel(TimePointModel model) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(
                realm1 -> {
                    realm1.copyToRealmOrUpdate(model);
                },
                () -> {
                    Log.d("UPDATE_DB", "UPDATE_DB true");
                },
                error -> {
                    Log.d("ERROR_DB", error.getMessage());
                });
    }

    public static String refactorTime(int time_HH, int time_MM) {
        StringBuilder builder = new StringBuilder();
        if (time_HH < 10) {
            builder.append("0");
        }
        builder.append(time_HH);
        builder.append(":");
        if (time_MM < 10) {
            builder.append("0");
        }
        builder.append(time_MM);
        return builder.toString();
    }

    public static int getSum(int data_1, int data_2, int data_3, int data_4, int data_5,
                             int data_6, int data_7, int data_8, int data_9, int data_10,
                             int data_11, int data_12) {

        int res = 0;
        int[] datas = new int[]{data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8, data_9, data_10, data_11, data_12};
        for (int i = 0; i < datas.length; i++) {
            int val = datas[i];
            if (val > 127) {
                  val =val - 128;
                  val =val / 10;
            }
            res = res + val;
        }
//        int res = data_1 + data_2 + data_3 + data_4 + data_5 + data_6 + data_7 +
//                data_8 + data_9 + data_10 + data_11 + data_12;
        if (res > 0) {
            return res / SettingsApp.getInstance().getCanelController();
        }
        return res;
    }
}

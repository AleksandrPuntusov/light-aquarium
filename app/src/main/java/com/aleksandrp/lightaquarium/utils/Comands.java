package com.aleksandrp.lightaquarium.utils;

import android.util.Log;

import com.aleksandrp.lightaquarium.db.TimePointModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Comands {

    /**
     * Протокол обмена между телефоном и контроллером освещения аквариума
     * <p>
     * Инициатором обмена всегда является программа на телефоне.
     * Обмен происходит пакетами до 20 байт.
     * Скорость передачи 9600 для начала, там будет видно.
     * Возможно подтверждение приема, если нужно. Эти вопросы обсудим в процессе, если будут сбои.
     * Структура пакета
     * Байт 1 – стартовый. Для телефона это 01h
     * Для контроллера это 81h
     * <p>
     * Байт 2 – идентификатор пакета. (тип передаваемых данных)
     * Байт 3 – длина данных (от 0 до 16)
     * Байт 4-19 собственно пакет данных
     * Последний байт – контрольная сумма (младший байт суммы всего пакета)
     * Итого не более 20 байт.
     * <p>
     * ===================================================================
     * <p>
     * Описание идентификаторов
     * 00h – запрос конфигурации контроллера .
     * В ответ устройство сообщает количество каналов, дальше можно расширить .
     * Пример запроса 01h 00h 00h CRC
     * Пример ответа 81h 00h 01h 04h CRC 4 канала
     * Ответ контроллера обязательный.
     * 01h – передача данных о первой временной точке.
     * Пример передачи 01h 01h 06h HH MM V1 V2 V4 V4 CRC 4 канала
     * HH MM – время точки
     * V1 V2 V4 V4 – уровни якрости для каналов
     * Пример ответа 81h 01h 00h CRC
     * Ответ контроллера обязательный.
     * 02h..18h – соответственно данные об остальных точках (до 24 штук).
     * 19h-1Fh - резерв на случай расширения.
     * 20h – мгновенное включение светильника на уровни мощности из пакета
     * Пример передачи 01h 20h 04h 10h 15h 20h 25h CRC
     * Зеленым мощности каналов 1,2,3,4 соответственно
     * Пример ответа 81h 20h 00h CRC
     * Ответ контроллера не обязательный
     * 21h – запрос программы из контроллера
     * Пример запроса 01h 21h 00 CRC
     * После запроса контроллер передает данные по каждой точке .
     * Подтверждение от телефона думаю не нужно, просто небольшая пауза между пакетами.
     * Пример ответа 81h 01h 04h V1 V2 V3 V4 CRC данные о первой точке, 4 канала.
     * 81h 02h 04h D1 D2 D3 D4 CRC данные о второй точке, 4 канала.
     * И так выдает данные о всех точках по порядку.
     * 22h – передача реального времени.
     * Пример передачи 01h 22 03h 08h 05h 00h CRC время 8.05.00
     * Пример ответа 81h 22h 00h CRC
     * Ответ не обязательный .
     * Для данной конфигурации думаю достаточно для работы.
     * По мере добавления внешних устройств просто добавляются идентификаторы с жестким форматом внутри…
     * Основной алгоритм остается неизменным.
     * <p>
     * Формат данных яркости
     * В контроллере яркость регулируется посредством PWM с дискретностью 1000.
     * В процентах получается шаг 0.1 %.
     * Уровень яркости в телефоне кодируется в диапазоне от 0 до100 с шагом 1. Далее контроллер преобразует его
     * в шаг 0.1%, умножает на 10.
     * Уровень яркости для ночной подсветки в телефоне кодируется так же от 0 до 100, с шагом 1, но контроллер эти данные
     * Преобразует в диапазон от 0 до 100 шагов PWM. В итоге получаю ночную подсветку до 10% с шагом 0.1%.
     * Это обеспечивает необходимый диапазон регулирования.
     * Предлагаю в программе телефона, при настройке якрости канала, добавить пункт, что это именно ночная подсветка, а не
     * обычный режим работы (например поставить галочку). При этом настройка яркости происходит так же. Но уже
     * контроллер видит это как ночная подсветка.
     * Практически предлагаю так. Т.к. старший бит у нас всегда равен нулю, ведь данные яркости до 100, то его можно
     * Использовать как признак ночной подсветки. Если он в единице, то контроллер его понимает как ночная подсветка
     * от 0 до 100 шагов PWM. Если в нуле, то умножает данные на 10 и получает яркость в шагах PWM.
     * Пример
     * 00000011 - это у нас яркость 3%, или 30 шагов PWM.
     * 10000011 - это уже ночная подсветка, и принимается яркость 3 шага.
     */

    public static final String FIRST_COMMAND = buildFirstRequest();           // 00h – запрос конфигурации контроллера .
    public static final String ENABLE_COMMAND = "01h 20h ";          // 20h – мгновенное включение светильника на уровни мощности из пакета
    public static final String GET_PROGRAM_COMMAND = buildGetPrograms(0);    // 21h – запрос программы из контроллера
    public static final String TIME_COMMAND = buildCurrentTime();            // 22h – передача реального времени. Пример передачи 01h 22 03h 08h 05h 00h CRC время 8.05.00

    private static String buildFirstRequest() {
        return "01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01";
    }

    public static String buildGetPrograms(int curentPoint) {
        StringBuilder builder = new StringBuilder("01 ");
        builder.append(decodeInt(curentPoint));
        builder.append("00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ");
        int sum = 1 + curentPoint;
        if (sum < 10) {
            builder.append("0");
        }
        builder.append(sum);
        return builder.toString();
    }

    public static String buildPointPrograms(TimePointModel model, int identificator, int countPoints) {
        int hours = parseInteger(model.getTime().split(":")[0], false);
        int minute = parseInteger(model.getTime().split(":")[1], false);
        int chanal_1 = parseInteger(model.getPort_value_1(), model.isIs_night_1());
        int chanal_2 = parseInteger(model.getPort_value_2(), model.isIs_night_2());
        int chanal_3 = parseInteger(model.getPort_value_3(), model.isIs_night_3());
        int chanal_4 = parseInteger(model.getPort_value_4(), model.isIs_night_4());
        int chanal_5 = parseInteger(model.getPort_value_5(), model.isIs_night_5());
        int chanal_6 = parseInteger(model.getPort_value_6(), model.isIs_night_6());
        int chanal_7 = parseInteger(model.getPort_value_7(), model.isIs_night_7());
        int chanal_8 = parseInteger(model.getPort_value_8(), model.isIs_night_8());
        int chanal_9 = parseInteger(model.getPort_value_9(), model.isIs_night_9());
        int chanal_10 = parseInteger(model.getPort_value_10(), model.isIs_night_10());
        int chanal_11 = parseInteger(model.getPort_value_11(), model.isIs_night_11());
        int chanal_12 = parseInteger(model.getPort_value_12(), model.isIs_night_12());
        int chanal_13 = model.getPort_value_general();

        int lenghtPackadge = 0;
        if (countPoints > 0) ++lenghtPackadge;
        if (hours > 0) ++lenghtPackadge;
        if (minute > 0) ++lenghtPackadge;
        if (chanal_1 > 0) ++lenghtPackadge;
        if (chanal_2 > 0) ++lenghtPackadge;
        if (chanal_3 > 0) ++lenghtPackadge;
        if (chanal_4 > 0) ++lenghtPackadge;
        if (chanal_5 > 0) ++lenghtPackadge;
        if (chanal_6 > 0) ++lenghtPackadge;
        if (chanal_7 > 0) ++lenghtPackadge;
        if (chanal_8 > 0) ++lenghtPackadge;
        if (chanal_9 > 0) ++lenghtPackadge;
        if (chanal_10 > 0) ++lenghtPackadge;
        if (chanal_11 > 0) ++lenghtPackadge;
        if (chanal_12 > 0) ++lenghtPackadge;
        if (chanal_13 > 0) ++lenghtPackadge;

        int sum = 1
                + identificator
                + lenghtPackadge
                + countPoints
                + hours
                + minute
                + chanal_1
                + chanal_2
                + chanal_3
                + chanal_4
                + chanal_5
                + chanal_6
                + chanal_7
                + chanal_8
                + chanal_9
                + chanal_10
                + chanal_11
                + chanal_12
                + chanal_13;

        StringBuilder builder = new StringBuilder("01 ");
        builder.append(
                decodeInt(identificator));
        builder.append(
                decodeInt(lenghtPackadge));
        builder.append(
                decodeInt(countPoints));
        builder.append(
                decodeInt(hours));
        builder.append(
                decodeInt(minute));
        builder.append(
                decodeInt(chanal_1));
        builder.append(
                decodeInt(chanal_2));
        builder.append(
                decodeInt(chanal_3));
        builder.append(
                decodeInt(chanal_4));
        builder.append(
                decodeInt(chanal_5));
        builder.append(
                decodeInt(chanal_6));
        builder.append(
                decodeInt(chanal_7));
        builder.append(
                decodeInt(chanal_8));
        builder.append(
                decodeInt(chanal_9));
        builder.append(
                decodeInt(chanal_10));
        builder.append(
                decodeInt(chanal_11));
        builder.append(
                decodeInt(chanal_12));
        builder.append(
                decodeInt(chanal_13));
        builder.append(
                decodeInt(sum));
        return builder.toString();
    }

    public static String buildPointProgramForShowString(String time) {
        int identificator = 31;
        int lenghtPackadge = 2;
        int hh = parseInteger(time.split(" ")[0], false);
        int mm = parseInteger(time.split(" ")[1], false);

        int sum = 1
                + identificator
                + lenghtPackadge
                + hh
                + mm;

        StringBuilder builder = new StringBuilder("01 ");
        builder.append(
                decodeInt(identificator));
        builder.append(
                decodeInt(lenghtPackadge));
        builder.append(
                decodeInt(hh));
        builder.append(
                decodeInt(mm));
        builder.append("00 00 00 00 00 00 00 00 00 00 00 00 00 00 ");
        builder.append(
                decodeInt(sum));
        Log.d("BLE_TAG", "command prev " + builder.toString());
        return builder.toString();
    }

    public static String buildPointProgramForShow(TimePointModel model) {
        int identificator = 32;
        int chanal_1 = parseInteger(model.getPort_value_1(), true);
        int chanal_2 = parseInteger(model.getPort_value_2(), true);
        int chanal_3 = parseInteger(model.getPort_value_3(), true);
        int chanal_4 = parseInteger(model.getPort_value_4(), true);
        int chanal_5 = parseInteger(model.getPort_value_5(), true);
        int chanal_6 = parseInteger(model.getPort_value_6(), true);
        int chanal_7 = parseInteger(model.getPort_value_7(), true);
        int chanal_8 = parseInteger(model.getPort_value_8(), true);
        int chanal_9 = parseInteger(model.getPort_value_9(), true);
        int chanal_10 = parseInteger(model.getPort_value_10(), true);
        int chanal_11 = parseInteger(model.getPort_value_11(), true);
        int chanal_12 = parseInteger(model.getPort_value_12(), true);
        int chanal_13 = 0;

        int lenghtPackadge = 0;
        if (chanal_1 > 0) ++lenghtPackadge;
        if (chanal_2 > 0) ++lenghtPackadge;
        if (chanal_3 > 0) ++lenghtPackadge;
        if (chanal_4 > 0) ++lenghtPackadge;
        if (chanal_5 > 0) ++lenghtPackadge;
        if (chanal_6 > 0) ++lenghtPackadge;
        if (chanal_7 > 0) ++lenghtPackadge;
        if (chanal_8 > 0) ++lenghtPackadge;
        if (chanal_9 > 0) ++lenghtPackadge;
        if (chanal_10 > 0) ++lenghtPackadge;
        if (chanal_11 > 0) ++lenghtPackadge;
        if (chanal_12 > 0) ++lenghtPackadge;
        if (chanal_13 > 0) ++lenghtPackadge;

        int sum = 1
                + identificator
                + lenghtPackadge
                + chanal_1
                + chanal_2
                + chanal_3
                + chanal_4
                + chanal_5
                + chanal_6
                + chanal_7
                + chanal_8
                + chanal_9
                + chanal_10
                + chanal_11
                + chanal_12
                + chanal_13;

        StringBuilder builder = new StringBuilder("01 ");
        builder.append(
                decodeInt(identificator));
        builder.append(
                decodeInt(lenghtPackadge));
        builder.append(
                decodeInt(chanal_1));
        builder.append(
                decodeInt(chanal_2));
        builder.append(
                decodeInt(chanal_3));
        builder.append(
                decodeInt(chanal_4));
        builder.append(
                decodeInt(chanal_5));
        builder.append(
                decodeInt(chanal_6));
        builder.append(
                decodeInt(chanal_7));
        builder.append(
                decodeInt(chanal_8));
        builder.append(
                decodeInt(chanal_9));
        builder.append(
                decodeInt(chanal_10));
        builder.append(
                decodeInt(chanal_11));
        builder.append(
                decodeInt(chanal_12));
        builder.append(
                decodeInt(chanal_13));
        builder.append("00 00 00 ");
        builder.append(
                decodeInt(sum));
        return builder.toString();
    }

    private static int parseInteger(String value, boolean isNight) {
        if (value == null || value.isEmpty()) {
            value = "0";
        }
        int parseInt = Integer.parseInt(value);
        if (isNight) {
//            parseInt = parseInt * 10;
        }
        return parseInt;
    }

    private static String buildCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        int hh = Integer.parseInt(formattedDate.split(":")[0]);
        int mm = Integer.parseInt(formattedDate.split(":")[1]);
        int ss = Integer.parseInt(formattedDate.split(":")[2]);

        formattedDate = getStringTime(hh, mm, ss);
        StringBuilder builder = new StringBuilder("01 25 03 ");
        builder.append(formattedDate);
        builder.append("00 00 00 00 00 00 00 00 00 00 00 00 00 ");
        builder.append((29 + hh + mm + ss));
        Log.d("CURRENT_TIME", builder.toString());
        return builder.toString();
    }

    private static String getStringTime(int hh, int mm, int ss) {
        StringBuilder builder = new StringBuilder();
        if (hh < 10) {
            builder.append("0");
        }
        builder.append(hh);
        builder.append(" ");
        if (mm < 10) {
            builder.append("0");
        }
        builder.append(mm);
        builder.append(" ");
        if (ss < 10) {
            builder.append("0");
        }
        builder.append(ss);
        builder.append(" ");

        return builder.toString();
    }


    public enum ComandsEnum {
        START_COMMAND,
        CHECK_TIME_COMMAND,
        SEND_PROGRAM_TO_CONTROLLER,
        SEND_PROGRAM_FOR_SHOW,
        SEND_POINT_FOR_SHOW,
        GET_PROGRAMS_COMMAND
    }

    public static final int IDENTFICATOR_CONFIG_CONTROLER_00 = 0;

    public static final String IDENTFICATOR_POINT_01 = "01";
    public static final String IDENTFICATOR_POINT_02 = "02";
    public static final String IDENTFICATOR_POINT_03 = "03";
    public static final String IDENTFICATOR_POINT_04 = "04";
    public static final String IDENTFICATOR_POINT_05 = "05";
    public static final String IDENTFICATOR_POINT_06 = "06";
    public static final String IDENTFICATOR_POINT_07 = "07";
    public static final String IDENTFICATOR_POINT_08 = "08";
    public static final String IDENTFICATOR_POINT_09 = "09";
    public static final String IDENTFICATOR_POINT_10 = "10";
    public static final String IDENTFICATOR_POINT_11 = "11";
    public static final String IDENTFICATOR_POINT_12 = "12";
    public static final String IDENTFICATOR_POINT_13 = "13";
    public static final String IDENTFICATOR_POINT_14 = "14";
    public static final String IDENTFICATOR_POINT_15 = "15";
    public static final String IDENTFICATOR_POINT_16 = "16";
    public static final String IDENTFICATOR_POINT_17 = "17";
    public static final String IDENTFICATOR_POINT_18 = "18";
    public static final String IDENTFICATOR_POINT_19 = "19";

    public static final int IDENTFICATOR_CHECK_TIME_COMMAND = 25;
    public static final int IDENTFICATOR_POWER_20 = 20;
    public static final int IDENTFICATOR_REQUEST_PROGRAMM_21 = 21;
    public static final int IDENTFICATOR_SEND_DATE_22 = 22;


    private static String decodeInt(int value) {
        String txt = "";
        if (value < 10) {
            txt = "0";
        }
        return txt + value + " ";
    }
}

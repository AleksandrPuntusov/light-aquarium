package com.aleksandrp.lightaquarium.utils;

public class Constants {

    public static final int PERMISSION_REQUEST_CODE = 201;
    public static final int REQUEST_CODE_LOC = 1;
    public static final int REQ_ENABLE_BT = 10;
    public static final int RESULT_SETTINGS = 111;
    public static final int BT_BOUNDED = 21;
    public static final int BT_SEARCH = 22;

    public static final int LED_RED = 30;
    public static final int LED_GREEN = 31;

    public static final int DELAY_TIMER = 2000;
}

package com.aleksandrp.lightaquarium.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.aleksandrp.lightaquarium.App;

public class SettingsApp {

    /**
     * Instance of SharedPreferences object
     */
    private SharedPreferences sPref;
    /**
     * Editor of SharedPreferences object
     */
    private SharedPreferences.Editor editor;

    private static SettingsApp ourInstance = new SettingsApp();

    /**
     * get instance settingsApp
     *
     * @return
     */
    public static SettingsApp getInstance() {
        return ourInstance;
    }

    /**
     * Construct the instance of the object
     */
    public SettingsApp() {
        sPref = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        editor = sPref.edit();
    }

    // Keys for opening settings from xml file
    private static final String KEY_CHANAL_CONTROLLER = "KEY_CANAL_CONTROLLER";
    private static final String KEY_POINTS_CONTROLLER = "KEY_POINTS_CONTROLLER";
    private static final String KEY_STEP_SHOW_PROGRAM = "KEY_STEP_SHOW_PROGRAM";


    public int getCanelController() {
//        return 4;
        return sPref.getInt(KEY_CHANAL_CONTROLLER, 0);
    }

    public void setCanelController(int ip) {
        editor.putInt(KEY_CHANAL_CONTROLLER, ip).commit();
    }

    public int getPoints() {
        return sPref.getInt(KEY_POINTS_CONTROLLER, 0);
    }

    public void setPoints(int ip) {
        editor.putInt(KEY_POINTS_CONTROLLER, ip).commit();
    }
    public int getStep() {
        return sPref.getInt(KEY_STEP_SHOW_PROGRAM, 1);
    }

    public void setStep(int ip) {
        editor.putInt(KEY_STEP_SHOW_PROGRAM, ip).commit();
    }
//    =======================================================

//    public long getTimeDeadline() {
//        return 1574711004000L;  // Thu, 17 Oct 2019 19:43:24 GMT
//    }
}

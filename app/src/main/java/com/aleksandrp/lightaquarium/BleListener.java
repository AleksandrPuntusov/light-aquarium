package com.aleksandrp.lightaquarium;

import com.aleksandrp.lightaquarium.ble.BtListAdapter;

public interface BleListener {

    void showProgress(boolean show);

    void setAdapterListView(BtListAdapter listAdapter);

    void registerReceiver();

    void showFrameControls();

    void showFrameLedControls();

    void setChecked(boolean b);

    void updateDataBle();

    void updateDataFromDb();
}

package com.aleksandrp.lightaquarium.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.aleksandrp.lightaquarium.R;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
    }
}

package com.aleksandrp.lightaquarium.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aleksandrp.lightaquarium.R;
import com.aleksandrp.lightaquarium.db.TimePointModel;
import com.aleksandrp.lightaquarium.utils.SettingsApp;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.aleksandrp.lightaquarium.db.DbHelperImpl.getSum;
import static com.aleksandrp.lightaquarium.db.DbHelperImpl.refactorTime;

public class SetValueDialog extends AlertDialog {

    @BindView(R.id.seekBar1)
    SeekBar seek_value_1;
    @BindView(R.id.seekBar2)
    SeekBar seek_value_2;
    @BindView(R.id.seekBar3)
    SeekBar seek_value_3;
    @BindView(R.id.seekBar4)
    SeekBar seek_value_4;
    @BindView(R.id.seekBar5)
    SeekBar seek_value_5;
    @BindView(R.id.seekBar6)
    SeekBar seek_value_6;
    @BindView(R.id.seekBar7)
    SeekBar seek_value_7;
    @BindView(R.id.seekBar8)
    SeekBar seek_value_8;
    @BindView(R.id.seekBar9)
    SeekBar seek_value_9;
    @BindView(R.id.seekBar10)
    SeekBar seek_value_10;
    @BindView(R.id.seekBar11)
    SeekBar seek_value_11;
    @BindView(R.id.seekBar12)
    SeekBar seek_value_12;

    @BindView(R.id.tv_value1)
    TextView tv_seek_value_1;
    @BindView(R.id.tv_value2)
    TextView tv_seek_value_2;
    @BindView(R.id.tv_value3)
    TextView tv_seek_value_3;
    @BindView(R.id.tv_value4)
    TextView tv_seek_value_4;
    @BindView(R.id.tv_value5)
    TextView tv_seek_value_5;
    @BindView(R.id.tv_value6)
    TextView tv_seek_value_6;
    @BindView(R.id.tv_value7)
    TextView tv_seek_value_7;
    @BindView(R.id.tv_value8)
    TextView tv_seek_value_8;
    @BindView(R.id.tv_value9)
    TextView tv_seek_value_9;
    @BindView(R.id.tv_value10)
    TextView tv_seek_value_10;
    @BindView(R.id.tv_value11)
    TextView tv_seek_value_11;
    @BindView(R.id.tv_value12)
    TextView tv_seek_value_12;

    @BindView(R.id.ll_canal_1)
    View ll_view_1;
    @BindView(R.id.ll_canal_2)
    View ll_view_2;
    @BindView(R.id.ll_canal_3)
    View ll_view_3;
    @BindView(R.id.ll_canal_4)
    View ll_view_4;
    @BindView(R.id.ll_canal_5)
    View ll_view_5;
    @BindView(R.id.ll_canal_6)
    View ll_view_6;
    @BindView(R.id.ll_canal_7)
    View ll_view_7;
    @BindView(R.id.ll_canal_8)
    View ll_view_8;
    @BindView(R.id.ll_canal_9)
    View ll_view_9;
    @BindView(R.id.ll_canal_10)
    View ll_view_10;
    @BindView(R.id.ll_canal_11)
    View ll_view_11;
    @BindView(R.id.ll_canal_12)
    View ll_view_12;

    @BindView(R.id.ll_seek_value)
    View ll_seek_value;

    @BindView(R.id.time_picker)
    TimePicker time_picker;

    private Context mContext;
    private SetValueDialogListener listener;
    private int hour = 0;
    private int minute = 0;

    public SetValueDialog(Context mContext, SetValueDialogListener listener) {
        super(mContext);
        this.mContext = mContext;
        this.listener = listener;
        Calendar mcurrentTime = Calendar.getInstance();
        hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mcurrentTime.get(Calendar.MINUTE);

        // Init dialog view
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_set_value, null);
        ButterKnife.bind(this, view);
        ll_seek_value.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            time_picker.setHour(hour);
            time_picker.setMinute(minute);
        } else {
            time_picker.setCurrentHour(hour);
            time_picker.setCurrentMinute(minute);
        }
        time_picker.setIs24HourView(true);
        time_picker.setOnTimeChangedListener((view1, hourOfDay, minute) -> {
            this.hour = hourOfDay;
            this.minute = minute;
        });

        tv_seek_value_1.setEnabled(false);
        tv_seek_value_1.setBackground(null);
        tv_seek_value_2.setEnabled(false);
        tv_seek_value_2.setBackground(null);
        tv_seek_value_3.setEnabled(false);
        tv_seek_value_3.setBackground(null);
        tv_seek_value_4.setEnabled(false);
        tv_seek_value_4.setBackground(null);
        tv_seek_value_5.setEnabled(false);
        tv_seek_value_5.setBackground(null);
        tv_seek_value_6.setEnabled(false);
        tv_seek_value_6.setBackground(null);
        tv_seek_value_7.setEnabled(false);
        tv_seek_value_7.setBackground(null);
        tv_seek_value_8.setEnabled(false);
        tv_seek_value_8.setBackground(null);
        tv_seek_value_9.setEnabled(false);
        tv_seek_value_9.setBackground(null);
        tv_seek_value_10.setEnabled(false);
        tv_seek_value_10.setBackground(null);
        tv_seek_value_11.setEnabled(false);
        tv_seek_value_11.setBackground(null);
        tv_seek_value_12.setEnabled(false);
        tv_seek_value_12.setBackground(null);

        seek_value_1.setOnSeekBarChangeListener(changeListener);
        seek_value_2.setOnSeekBarChangeListener(changeListener);
        seek_value_3.setOnSeekBarChangeListener(changeListener);
        seek_value_4.setOnSeekBarChangeListener(changeListener);
        seek_value_5.setOnSeekBarChangeListener(changeListener);
        seek_value_6.setOnSeekBarChangeListener(changeListener);
        seek_value_7.setOnSeekBarChangeListener(changeListener);
        seek_value_8.setOnSeekBarChangeListener(changeListener);
        seek_value_9.setOnSeekBarChangeListener(changeListener);
        seek_value_10.setOnSeekBarChangeListener(changeListener);
        seek_value_11.setOnSeekBarChangeListener(changeListener);
        seek_value_12.setOnSeekBarChangeListener(changeListener);
        switch (SettingsApp.getInstance().getCanelController()) {
            case 12:
                ll_view_12.setVisibility(View.VISIBLE);
            case 11:
                ll_view_11.setVisibility(View.VISIBLE);
            case 10:
                ll_view_10.setVisibility(View.VISIBLE);
            case 9:
                ll_view_9.setVisibility(View.VISIBLE);
            case 8:
                ll_view_8.setVisibility(View.VISIBLE);
            case 7:
                ll_view_7.setVisibility(View.VISIBLE);
            case 6:
                ll_view_6.setVisibility(View.VISIBLE);
            case 5:
                ll_view_5.setVisibility(View.VISIBLE);
            case 4:
                ll_view_4.setVisibility(View.VISIBLE);
            case 3:
                ll_view_3.setVisibility(View.VISIBLE);
            case 2:
                ll_view_2.setVisibility(View.VISIBLE);
            case 1:
                ll_view_1.setVisibility(View.VISIBLE);
                break;
        }

        this.setView(view);
    }

    @OnClick(R.id.ok_set_value)
    public void clickOk() {
        if (listener != null) {
            TimePointModel model = new TimePointModel();
            model.setTime(refactorTime(hour, minute));
            model.setPort_value_1(String.valueOf(seek_value_1.getProgress()));
            model.setPort_value_2(String.valueOf(seek_value_2.getProgress()));
            model.setPort_value_3(String.valueOf(seek_value_3.getProgress()));
            model.setPort_value_4(String.valueOf(seek_value_4.getProgress()));
            model.setPort_value_5(String.valueOf(seek_value_5.getProgress()));
            model.setPort_value_6(String.valueOf(seek_value_6.getProgress()));
            model.setPort_value_7(String.valueOf(seek_value_7.getProgress()));
            model.setPort_value_8(String.valueOf(seek_value_8.getProgress()));
            model.setPort_value_9(String.valueOf(seek_value_9.getProgress()));
            model.setPort_value_10(String.valueOf(seek_value_10.getProgress()));
            model.setPort_value_11(String.valueOf(seek_value_11.getProgress()));
            model.setPort_value_12(String.valueOf(seek_value_12.getProgress()));

            int sum = getSum(seek_value_1.getProgress(),
                    seek_value_2.getProgress(),
                    seek_value_3.getProgress(),
                    seek_value_4.getProgress(),
                    seek_value_5.getProgress(),
                    seek_value_6.getProgress(),
                    seek_value_7.getProgress(),
                    seek_value_8.getProgress(),
                    seek_value_9.getProgress(),
                    seek_value_10.getProgress(),
                    seek_value_11.getProgress(),
                    seek_value_12.getProgress());

            model.setPort_value_general(sum);
            listener.returnValue(hour, minute, model);
        }
        cancel();
    }

    SeekBar.OnSeekBarChangeListener changeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            switch (seekBar.getId()) {
                case R.id.seekBar1:
                    tv_seek_value_1.setText(progress + "%");
                    break;
                case R.id.seekBar2:
                    tv_seek_value_2.setText(progress + "%");
                    break;
                case R.id.seekBar3:
                    tv_seek_value_3.setText(progress + "%");
                    break;
                case R.id.seekBar4:
                    tv_seek_value_4.setText(progress + "%");
                    break;
                case R.id.seekBar5:
                    tv_seek_value_5.setText(progress + "%");
                    break;
                case R.id.seekBar6:
                    tv_seek_value_6.setText(progress + "%");
                    break;
                case R.id.seekBar7:
                    tv_seek_value_7.setText(progress + "%");
                    break;
                case R.id.seekBar8:
                    tv_seek_value_8.setText(progress + "%");
                    break;
                case R.id.seekBar9:
                    tv_seek_value_9.setText(progress + "%");
                    break;
                case R.id.seekBar10:
                    tv_seek_value_10.setText(progress + "%");
                    break;
                case R.id.seekBar11:
                    tv_seek_value_11.setText(progress + "%");
                    break;
                case R.id.seekBar12:
                    tv_seek_value_12.setText(progress + "%");
                    break;
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    public interface SetValueDialogListener {
        void returnValue(int hour, int minute, TimePointModel model);
    }
}
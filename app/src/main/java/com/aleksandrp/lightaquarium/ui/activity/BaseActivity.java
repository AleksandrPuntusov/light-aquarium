package com.aleksandrp.lightaquarium.ui.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.aleksandrp.lightaquarium.BleListener;
import com.aleksandrp.lightaquarium.ble.BleHelper;
import com.aleksandrp.lightaquarium.ble.BtListAdapter;

import java.util.Calendar;

import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity
        implements BleListener {

    public BleHelper bleHelper;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            switch (action) {
                case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                    bleACTION_DISCOVERY_STARTED();
                    break;
                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    bleACTION_DISCOVERY_FINISHED();
                    break;
                case BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    bleHelper.addDevice(device);
                    break;
            }
        }
    };

    public int getResLayout() {
        return 0;
    }

    public void initUi() {

    }

    public void updateData() {

    }

    public void updateFromDb() {

    }

    public void bleACTION_DISCOVERY_STARTED() {

    }

    public void bleACTION_DISCOVERY_FINISHED() {

    }

    public void updateTime() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        long time = Calendar.getInstance().getTime().getTime();
        Log.d("CURRENT_", "current = $time");
//        if (time >= SettingsApp.getInstance().getTimeDeadline()) {
//            Toast.makeText(this, "Защита разработчика", Toast.LENGTH_SHORT).show();
//            new Handler().postDelayed(() -> {
//                finish();
//                System.exit(0);
//            }, 2000);
//        }

        setContentView(getResLayout());
        ButterKnife.bind(this);
        bleHelper = new BleHelper(this, this);
        initUi();
    }

    @Override
    protected void onStop() {
        super.onStop();
        bleHelper.pause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        bleHelper.checkBluetooth();
        new Handler().postDelayed(() -> {
            bleHelper.resume();
        }, 500);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        bleHelper.destroy();
    }

    @Override
    public void showProgress(boolean show) {

    }

    @Override
    public void setAdapterListView(BtListAdapter listAdapter) {

    }

    @Override
    public void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        registerReceiver(receiver, filter);
    }

    @Override
    public void showFrameControls() {

    }

    @Override
    public void showFrameLedControls() {

    }

    @Override
    public void setChecked(boolean b) {

    }

    @Override
    public void updateDataBle() {
        updateData();
    }

    @Override
    public void updateDataFromDb() {
        updateFromDb();
    }

}

package com.aleksandrp.lightaquarium.ui.presenter;

import android.content.pm.PackageManager;

import com.aleksandrp.lightaquarium.ui.activity.MainActivity;
import com.aleksandrp.lightaquarium.ui.presenter.view.MainActivityView;

public class MainActivityPresenter {

    private MainActivityView view;
    private MainActivity activity;

    public MainActivityPresenter(MainActivityView view, MainActivity activity) {
        this.view = view;
        this.activity = activity;
    }

    public boolean hasBLE() {
        boolean b = activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        if (!b) {
            view.showMessage("На устройстве отсутствует блютуз модуль!");
        }
        return b;
    }
}

package com.aleksandrp.lightaquarium.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.aleksandrp.lightaquarium.R;

public class ProgressDialogApp extends AlertDialog {

    public ProgressDialogApp(Context mContext) {
        super(mContext);
        // Init dialog view
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_progress, null);
        this.setView(view);
    }

    @Override
    public void cancel() {
    }
}

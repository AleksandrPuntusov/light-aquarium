package com.aleksandrp.lightaquarium.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {

    private Handler handler;
    private Runnable runnable;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getResView(), container, false);
        ButterKnife.bind(this, view);
        initUi(view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        startTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (handler != null) {
            handler.removeCallbacks(runnable);
            if (runnable != null) {
                runnable = null;
            }
        }
    }

    private void startTimer() {
        showTime();

        handler = new Handler();
        runnable = () -> startTimer();
        handler.postDelayed(runnable, 1000);
    }

    protected abstract void showTime();

    protected abstract int getResView();

    protected abstract void initUi(View view);

}

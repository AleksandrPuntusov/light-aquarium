package com.aleksandrp.lightaquarium.ui.presenter.view;

public interface MainActivityView {

    void showMessage(String msg);

}

package com.aleksandrp.lightaquarium.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.aleksandrp.lightaquarium.R;
import com.aleksandrp.lightaquarium.db.TimePointModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogListPoints extends AlertDialog {

    @BindView(R.id.lv_list_points_dialog)
    ListView lv_list_points_dialog;

    private Context mContext;
    private ArrayList<TimePointModel> timePointModels;
    private DialogListPointsListener listener;

    public DialogListPoints(Context mContext, ArrayList<TimePointModel> currentPoint, DialogListPointsListener listener) {
        super(mContext);
        this.mContext = mContext;
        this.timePointModels = currentPoint;
        this.listener = listener;

        // Init dialog view
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_list_points, null);
        ButterKnife.bind(this, view);

        setUi();
        this.setView(view);
    }


    public void setUi() {
        final String[] times = new String[timePointModels.size()];
        for (int i = 0; i < timePointModels.size(); i++) {
            TimePointModel model = timePointModels.get(i);
            times[i] = model.getTime();
        }

        // используем адаптер данных
        ArrayAdapter adapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_list_item_1, times);
        lv_list_points_dialog.setAdapter(adapter);
        lv_list_points_dialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                listener.returnTimePointModel(timePointModels.get(position));
                cancel();
            }
        });
    }

    public interface DialogListPointsListener {
        void returnTimePointModel(TimePointModel currentPoint);
    }
}
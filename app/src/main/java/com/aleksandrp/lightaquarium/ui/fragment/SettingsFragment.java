package com.aleksandrp.lightaquarium.ui.fragment;

import android.app.AlertDialog;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aleksandrp.lightaquarium.R;
import com.aleksandrp.lightaquarium.db.DbHelperImpl;
import com.aleksandrp.lightaquarium.db.TimePointModel;
import com.aleksandrp.lightaquarium.ui.activity.MainActivity;
import com.aleksandrp.lightaquarium.ui.dialog.AreYouSureDialog;
import com.aleksandrp.lightaquarium.ui.dialog.ProgressDialogApp;
import com.aleksandrp.lightaquarium.ui.dialog.SetValueDialog;
import com.aleksandrp.lightaquarium.utils.SettingsApp;
import com.jjoe64.graphview.GraphView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.aleksandrp.lightaquarium.db.DbHelperImpl.getSum;
import static com.aleksandrp.lightaquarium.db.DbHelperImpl.refactorTime;
import static com.aleksandrp.lightaquarium.shar.CharUtils.getDateDouble;
import static com.aleksandrp.lightaquarium.shar.CharUtils.initChars;
import static com.aleksandrp.lightaquarium.shar.CharUtils.setData;
import static com.aleksandrp.lightaquarium.shar.CharUtils.setPointListener;
import static com.aleksandrp.lightaquarium.shar.CharUtils.showPointChar;
import static com.aleksandrp.lightaquarium.utils.DateUtils.getCueentTime;

public class SettingsFragment extends BaseFragment
        implements SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.chart_time)
    GraphView chart_time;

    @BindView(R.id.cb_value_1)
    CheckBox cb_value_1;
    @BindView(R.id.cb_value_2)
    CheckBox cb_value_2;
    @BindView(R.id.cb_value_3)
    CheckBox cb_value_3;
    @BindView(R.id.cb_value_4)
    CheckBox cb_value_4;
    @BindView(R.id.cb_value_5)
    CheckBox cb_value_5;
    @BindView(R.id.cb_value_6)
    CheckBox cb_value_6;
    @BindView(R.id.cb_value_7)
    CheckBox cb_value_7;
    @BindView(R.id.cb_value_8)
    CheckBox cb_value_8;
    @BindView(R.id.cb_value_9)
    CheckBox cb_value_9;
    @BindView(R.id.cb_value_10)
    CheckBox cb_value_10;
    @BindView(R.id.cb_value_11)
    CheckBox cb_value_11;
    @BindView(R.id.cb_value_12)
    CheckBox cb_value_12;

    @BindView(R.id.ll_canal_1)
    View ll_canal_1;
    @BindView(R.id.ll_canal_2)
    View ll_canal_2;
    @BindView(R.id.ll_canal_3)
    View ll_canal_3;
    @BindView(R.id.ll_canal_4)
    View ll_canal_4;
    @BindView(R.id.ll_canal_5)
    View ll_canal_5;
    @BindView(R.id.ll_canal_6)
    View ll_canal_6;
    @BindView(R.id.ll_canal_7)
    View ll_canal_7;
    @BindView(R.id.ll_canal_8)
    View ll_canal_8;
    @BindView(R.id.ll_canal_9)
    View ll_canal_9;
    @BindView(R.id.ll_canal_10)
    View ll_canal_10;
    @BindView(R.id.ll_canal_11)
    View ll_canal_11;
    @BindView(R.id.ll_canal_12)
    View ll_canal_12;

    @BindView(R.id.seekBar1)
    SeekBar seekBar1;
    @BindView(R.id.seekBar2)
    SeekBar seekBar2;
    @BindView(R.id.seekBar3)
    SeekBar seekBar3;
    @BindView(R.id.seekBar4)
    SeekBar seekBar4;
    @BindView(R.id.seekBar5)
    SeekBar seekBar5;
    @BindView(R.id.seekBar6)
    SeekBar seekBar6;
    @BindView(R.id.seekBar7)
    SeekBar seekBar7;
    @BindView(R.id.seekBar8)
    SeekBar seekBar8;
    @BindView(R.id.seekBar9)
    SeekBar seekBar9;
    @BindView(R.id.seekBar10)
    SeekBar seekBar10;
    @BindView(R.id.seekBar11)
    SeekBar seekBar11;
    @BindView(R.id.seekBar12)
    SeekBar seekBar12;

    @BindView(R.id.tv_left_point)
    TextView tv_left_point;
    @BindView(R.id.tv_show_point)
    TextView tv_show_point;
    @BindView(R.id.tv_rigt_point)
    TextView tv_rigt_point;

    @BindView(R.id.tv_value1)
    TextView tv_value1;
    @BindView(R.id.tv_value2)
    TextView tv_value2;
    @BindView(R.id.tv_value3)
    TextView tv_value3;
    @BindView(R.id.tv_value4)
    TextView tv_value4;
    @BindView(R.id.tv_value5)
    TextView tv_value5;
    @BindView(R.id.tv_value6)
    TextView tv_value6;
    @BindView(R.id.tv_value7)
    TextView tv_value7;
    @BindView(R.id.tv_value8)
    TextView tv_value8;
    @BindView(R.id.tv_value9)
    TextView tv_value9;
    @BindView(R.id.tv_value10)
    TextView tv_value10;
    @BindView(R.id.tv_value11)
    TextView tv_value11;
    @BindView(R.id.tv_value12)
    TextView tv_value12;

    @BindView(R.id.tv_time_picker)
    TextView tv_time_picker;

    @BindView(R.id.tv_description_char_fragment)
    TextView tv_description_char;

    private int countCanal = 0;
    private int currentInt = 1;
    private ArrayList<TimePointModel> pointModels = new ArrayList<>();
    private TimePointModel currentPoint = null;
    private ProgressDialogApp progressDialog;
    private AlertDialog dialog;

    private MainActivity thisActivity;

    @Override
    protected void showTime() {
        updateTime();
    }

    @Override
    protected int getResView() {
        return R.layout.frament_settings;
    }

    @Override
    protected void initUi(View view) {
        thisActivity = (MainActivity) getActivity();
        countCanal = SettingsApp.getInstance().getCanelController();
        progressDialog = new ProgressDialogApp(getActivity());

        setChart();

        getDataFromDb();
        setCurrentTime();

        setVisibleGoneViews();
        seekBar1.setOnSeekBarChangeListener(this);
        seekBar2.setOnSeekBarChangeListener(this);
        seekBar3.setOnSeekBarChangeListener(this);
        seekBar4.setOnSeekBarChangeListener(this);
        seekBar5.setOnSeekBarChangeListener(this);
        seekBar6.setOnSeekBarChangeListener(this);
        seekBar7.setOnSeekBarChangeListener(this);
        seekBar8.setOnSeekBarChangeListener(this);
        seekBar9.setOnSeekBarChangeListener(this);
        seekBar10.setOnSeekBarChangeListener(this);
        seekBar11.setOnSeekBarChangeListener(this);
        seekBar12.setOnSeekBarChangeListener(this);

        setEnabledSeekBars(seekBar1, false, cb_value_1);
        setEnabledSeekBars(seekBar2, false, cb_value_2);
        setEnabledSeekBars(seekBar3, false, cb_value_3);
        setEnabledSeekBars(seekBar4, false, cb_value_4);
        setEnabledSeekBars(seekBar5, false, cb_value_5);
        setEnabledSeekBars(seekBar6, false, cb_value_6);
        setEnabledSeekBars(seekBar7, false, cb_value_7);
        setEnabledSeekBars(seekBar8, false, cb_value_8);
        setEnabledSeekBars(seekBar9, false, cb_value_9);
        setEnabledSeekBars(seekBar10, false, cb_value_10);
        setEnabledSeekBars(seekBar11, false, cb_value_11);
        setEnabledSeekBars(seekBar12, false, cb_value_12);
    }

    private void setChart() {
        initChars(getActivity(), chart_time);
        setPointListener(x -> {
            for (int i = 0; i < pointModels.size(); i++) {
                TimePointModel model = pointModels.get(i);
                if (getDateDouble(model.getTime()) == x) {
                    currentInt = i - 1;
                    currentPoint = model;
                }
            }
            setCurrentTime();
            setHoursMinutes(currentPoint);
        });
    }

    @OnClick({R.id.cb_value_1, R.id.cb_value_2, R.id.cb_value_3,
            R.id.cb_value_4, R.id.cb_value_5, R.id.cb_value_6,
            R.id.cb_value_7, R.id.cb_value_8, R.id.cb_value_9,
            R.id.cb_value_10, R.id.cb_value_11, R.id.cb_value_12,
    })
    public void clickCheckBox(CheckBox view) {
        switch (view.getId()) {
            case R.id.cb_value_1:
            case R.id.cb_value_2:
            case R.id.cb_value_3:
            case R.id.cb_value_4:
            case R.id.cb_value_5:
            case R.id.cb_value_6:
            case R.id.cb_value_7:
            case R.id.cb_value_8:
            case R.id.cb_value_9:
            case R.id.cb_value_10:
            case R.id.cb_value_11:
            case R.id.cb_value_12:
                saveEditPointData();
                break;
        }
    }

    private void setEnabledSeekBars(SeekBar seekBar, boolean b, CheckBox cb) {
        b = true;
        seekBar.setEnabled(b);
        cb.setEnabled(b);

        setValueDefault();
    }

    private void setVisibleGoneViews() {
        if (currentPoint == null || currentPoint.getPort_value_1() == null) {
            getActivity().onBackPressed();
            return;
        }
        switch (countCanal) {
            case 12:
                ll_canal_12.setVisibility(View.VISIBLE);
                cb_value_12.setChecked(currentPoint.isIs_night_12());
                seekBar12.setProgress(getProgress(currentPoint.getPort_value_12(), currentPoint.isIs_night_12()));
            case 11:
                ll_canal_11.setVisibility(View.VISIBLE);
                cb_value_11.setChecked(currentPoint.isIs_night_11());
                seekBar8.setProgress(getProgress(currentPoint.getPort_value_11(), currentPoint.isIs_night_11()));
            case 10:
                ll_canal_10.setVisibility(View.VISIBLE);
                cb_value_10.setChecked(currentPoint.isIs_night_10());
                seekBar8.setProgress(getProgress(currentPoint.getPort_value_10(), currentPoint.isIs_night_10()));
            case 9:
                ll_canal_9.setVisibility(View.VISIBLE);
                cb_value_9.setChecked(currentPoint.isIs_night_9());
                seekBar8.setProgress(getProgress(currentPoint.getPort_value_9(), currentPoint.isIs_night_9()));
            case 8:
                ll_canal_8.setVisibility(View.VISIBLE);
                cb_value_8.setChecked(currentPoint.isIs_night_8());
                seekBar8.setProgress(getProgress(currentPoint.getPort_value_8(), currentPoint.isIs_night_8()));
            case 7:
                ll_canal_7.setVisibility(View.VISIBLE);
                cb_value_7.setChecked(currentPoint.isIs_night_7());
                seekBar7.setProgress(getProgress(currentPoint.getPort_value_7(), currentPoint.isIs_night_7()));
            case 6:
                ll_canal_6.setVisibility(View.VISIBLE);
                cb_value_6.setChecked(currentPoint.isIs_night_6());
                seekBar6.setProgress(getProgress(currentPoint.getPort_value_6(), currentPoint.isIs_night_6()));
            case 5:
                ll_canal_5.setVisibility(View.VISIBLE);
                cb_value_5.setChecked(currentPoint.isIs_night_5());
                seekBar5.setProgress(getProgress(currentPoint.getPort_value_5(), currentPoint.isIs_night_5()));
            case 4:
                ll_canal_4.setVisibility(View.VISIBLE);
                cb_value_4.setChecked(currentPoint.isIs_night_4());
                seekBar4.setProgress(getProgress(currentPoint.getPort_value_4(), currentPoint.isIs_night_4()));
            case 3:
                ll_canal_3.setVisibility(View.VISIBLE);
                cb_value_3.setChecked(currentPoint.isIs_night_4());
                seekBar3.setProgress(getProgress(currentPoint.getPort_value_3(), currentPoint.isIs_night_4()));
            case 2:
                ll_canal_2.setVisibility(View.VISIBLE);
                cb_value_2.setChecked(currentPoint.isIs_night_2());
                seekBar2.setProgress(getProgress(currentPoint.getPort_value_2(), currentPoint.isIs_night_2()));
            case 1:
                ll_canal_1.setVisibility(View.VISIBLE);
                cb_value_1.setChecked(currentPoint.isIs_night_1());
                seekBar1.setProgress(getProgress(currentPoint.getPort_value_1(), currentPoint.isIs_night_1()));
                break;
        }
    }

    private int getProgress(String s, boolean is_night) {
        if (s == null || s.isEmpty()) {
            return 0;
        }
        int parseInt = Integer.parseInt(s);
        if (is_night) {
            parseInt = parseInt - 128;
        }
        return parseInt;
    }

    private void setValueDefault() {
        setTextView(tv_value1, seekBar1.getProgress(), cb_value_1.isChecked());
        setTextView(tv_value2, seekBar2.getProgress(), cb_value_2.isChecked());
        setTextView(tv_value3, seekBar3.getProgress(), cb_value_3.isChecked());
        setTextView(tv_value4, seekBar4.getProgress(), cb_value_4.isChecked());
        setTextView(tv_value5, seekBar5.getProgress(), cb_value_5.isChecked());
        setTextView(tv_value6, seekBar6.getProgress(), cb_value_6.isChecked());
        setTextView(tv_value7, seekBar7.getProgress(), cb_value_7.isChecked());
        setTextView(tv_value8, seekBar8.getProgress(), cb_value_8.isChecked());
        setTextView(tv_value9, seekBar9.getProgress(), cb_value_9.isChecked());
        setTextView(tv_value10, seekBar10.getProgress(), cb_value_10.isChecked());
        setTextView(tv_value11, seekBar11.getProgress(), cb_value_11.isChecked());
        setTextView(tv_value12, seekBar12.getProgress(), cb_value_12.isChecked());
    }

    private void getDataFromDb() {
        pointModels = DbHelperImpl.getInstance().readAllPoints();
        updateChar();
        showProgress(false);
    }

    private void updateChar() {
        updateDataInChart(pointModels);
    }

    private void updateDataInChart(ArrayList<TimePointModel> pointModels) {
        Log.d("IST_POINTS", "IST_POINTS " + pointModels.toString());
        // add data
        setData(pointModels);
    }

    @OnClick({R.id.tv_rigt_point,
            R.id.tv_save_char,
            R.id.tv_remove_point,
            R.id.tv_add_new_point,
            R.id.tv_load_spectrum,
            R.id.iv_shoe_point,
            R.id.tv_left_point})
    public void onClickView(View v) {
        int size = pointModels.size();
        currentInt = Integer.parseInt(tv_show_point.getText().toString().trim());
        switch (v.getId()) {
            case R.id.tv_rigt_point:
                if (size > 0) {
                    if (currentInt >= size) {
                        currentInt = size;
                    } else {
                        ++currentInt;
                    }
                    if (pointModels.size() > 0 && pointModels.size() >= currentInt) {
                        setHoursMinutes(pointModels.get(currentInt - 1));
                    } else
                        setHoursMinutes(currentPoint);
                }
                break;
            case R.id.tv_left_point:
                if (size > 0) {
                    if (currentInt <= 1) {
                        currentInt = 1;
                    } else {
                        --currentInt;
                    }
                    setHoursMinutes(pointModels.get(currentInt - 1));
                }
                break;
            case R.id.tv_save_char:
                doSaveDataInController();
                break;
            case R.id.tv_remove_point:
                showDialogAreYouSure();
                break;
            case R.id.tv_add_new_point:
                showAddNewPoint();
                break;
            case R.id.tv_load_spectrum:
                showProgress(true);
                new Handler().postDelayed(() -> {
                    thisActivity.bleHelper.getProgramsFromController();
                }, 100);
                break;
            case R.id.iv_shoe_point:
                thisActivity.bleHelper.doSendPointToController(currentPoint);
                break;
        }
        tv_show_point.setText(String.valueOf(currentInt));
    }

    private void doSaveDataInController() {
        showProgress(true);
        new Handler().postDelayed(() -> {
            thisActivity.bleHelper.sentProgramsToController();
        }, 100);
    }

    private void setCurrentTime() {
        int size = pointModels.size();
        if (size > 0) {
            if (currentPoint == null) {
                currentPoint = pointModels.get(0);
            }
            tv_show_point.setText(String.valueOf(currentInt));
            showPointInChar();
            tv_time_picker.setText(currentPoint.getTime());
        } else {
            tv_time_picker.setText("--");
            tv_show_point.setText("0");
        }
    }

    private void setHoursMinutes(TimePointModel currentPoint) {
        this.currentPoint = currentPoint;
        showPointInChar();
        setVisibleGoneViews();
        tv_time_picker.setText(currentPoint.getTime());
    }

    private void showPointInChar() {
        showPointChar(currentPoint);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.seekBar1:
                setTextView(tv_value1, progress, cb_value_1.isChecked());
                break;
            case R.id.seekBar2:
                setTextView(tv_value2, progress, cb_value_2.isChecked());
                break;
            case R.id.seekBar3:
                setTextView(tv_value3, progress, cb_value_3.isChecked());
                break;
            case R.id.seekBar4:
                setTextView(tv_value4, progress, cb_value_4.isChecked());
                break;
            case R.id.seekBar5:
                setTextView(tv_value5, progress, cb_value_5.isChecked());
                break;
            case R.id.seekBar6:
                setTextView(tv_value6, progress, cb_value_6.isChecked());
                break;
            case R.id.seekBar7:
                setTextView(tv_value7, progress, cb_value_7.isChecked());
                break;
            case R.id.seekBar8:
                setTextView(tv_value8, progress, cb_value_8.isChecked());
                break;
            case R.id.seekBar9:
                setTextView(tv_value9, progress, cb_value_9.isChecked());
                break;
            case R.id.seekBar10:
                setTextView(tv_value10, progress, cb_value_10.isChecked());
                break;
            case R.id.seekBar11:
                setTextView(tv_value11, progress, cb_value_11.isChecked());
                break;
            case R.id.seekBar12:
                setTextView(tv_value12, progress, cb_value_12.isChecked());
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        switch (seekBar.getId()) {
            case R.id.seekBar1:
            case R.id.seekBar2:
            case R.id.seekBar3:
            case R.id.seekBar4:
            case R.id.seekBar5:
            case R.id.seekBar6:
            case R.id.seekBar7:
            case R.id.seekBar8:
            case R.id.seekBar9:
            case R.id.seekBar10:
            case R.id.seekBar11:
            case R.id.seekBar12:
                saveEditPointData();
                break;
        }
    }

    private void saveEditPointData() {
        boolean checked1 = cb_value_1.isChecked();
        boolean checked2 = cb_value_2.isChecked();
        boolean checked3 = cb_value_3.isChecked();
        boolean checked4 = cb_value_4.isChecked();
        boolean checked5 = cb_value_5.isChecked();
        boolean checked6 = cb_value_6.isChecked();
        boolean checked7 = cb_value_7.isChecked();
        boolean checked8 = cb_value_8.isChecked();
        boolean checked9 = cb_value_9.isChecked();
        boolean checked10 = cb_value_10.isChecked();
        boolean checked11 = cb_value_11.isChecked();
        boolean checked12 = cb_value_12.isChecked();

//        boolean[] checks = new boolean[]{
//                checked1, checked2, checked3, checked4, checked5, checked6,
//                checked7, checked8, checked9, checked10, checked11, checked12};
//        int[] progresses = new int[]{
//                seekBar1.getProgress(), seekBar2.getProgress(), seekBar3.getProgress(),
//                seekBar4.getProgress(), seekBar5.getProgress(), seekBar6.getProgress(),
//                seekBar7.getProgress(), seekBar8.getProgress(), seekBar9.getProgress(),
//                seekBar10.getProgress(), seekBar11.getProgress(), seekBar12.getProgress()};
//
//        int progress1 = checked1 ? (getProgressInNight(checks, progresses)) : seekBar1.getProgress();
//        int progress2 = checked2 ? (getProgressInNight(checks, progresses)) : seekBar2.getProgress();
//        int progress3 = checked3 ? (getProgressInNight(checks, progresses)) : seekBar3.getProgress();
//        int progress4 = checked4 ? (getProgressInNight(checks, progresses)) : seekBar4.getProgress();
//        int progress5 = checked5 ? (getProgressInNight(checks, progresses)) : seekBar5.getProgress();
//        int progress6 = checked6 ? (getProgressInNight(checks, progresses)) : seekBar6.getProgress();
//        int progress7 = checked7 ? (getProgressInNight(checks, progresses)) : seekBar7.getProgress();
//        int progress8 = checked8 ? (getProgressInNight(checks, progresses)) : seekBar8.getProgress();
//        int progress9 = checked9 ? (getProgressInNight(checks, progresses)) : seekBar9.getProgress();
//        int progress10 = checked10 ? (getProgressInNight(checks, progresses)) : seekBar10.getProgress();
//        int progress11 = checked11 ? (getProgressInNight(checks, progresses)) : seekBar11.getProgress();
//        int progress12 = checked12 ? (getProgressInNight(checks, progresses)) : seekBar12.getProgress();
//
        int progress1 = checked1 ? (seekBar1.getProgress() + 128) : seekBar1.getProgress();
        int progress2 = checked2 ? (seekBar2.getProgress() + 128) : seekBar2.getProgress();
        int progress3 = checked3 ? (seekBar3.getProgress() + 128) : seekBar3.getProgress();
        int progress4 = checked4 ? (seekBar4.getProgress() + 128) : seekBar4.getProgress();
        int progress5 = checked5 ? (seekBar5.getProgress() + 128) : seekBar5.getProgress();
        int progress6 = checked6 ? (seekBar6.getProgress() + 128) : seekBar6.getProgress();
        int progress7 = checked7 ? (seekBar7.getProgress() + 128) : seekBar7.getProgress();
        int progress8 = checked8 ? (seekBar8.getProgress() + 128) : seekBar8.getProgress();
        int progress9 = checked9 ? (seekBar9.getProgress() + 128) : seekBar9.getProgress();
        int progress10 = checked10 ? (seekBar10.getProgress() + 128) : seekBar10.getProgress();
        int progress11 = checked11 ? (seekBar11.getProgress() + 128) : seekBar11.getProgress();
        int progress12 = checked12 ? (seekBar12.getProgress() + 128) : seekBar12.getProgress();

        setNightProgress(checked1, seekBar1);
        setNightProgress(checked2, seekBar2);
        setNightProgress(checked1, seekBar3);
        setNightProgress(checked1, seekBar4);
        setNightProgress(checked1, seekBar5);
        setNightProgress(checked1, seekBar6);
        setNightProgress(checked1, seekBar7);
        setNightProgress(checked1, seekBar8);
        setNightProgress(checked1, seekBar9);
        setNightProgress(checked1, seekBar10);
        setNightProgress(checked1, seekBar11);
        setNightProgress(checked1, seekBar12);

        currentPoint.setIs_night_1(checked1);
        currentPoint.setIs_night_2(checked2);
        currentPoint.setIs_night_3(checked3);
        currentPoint.setIs_night_4(checked4);
        currentPoint.setIs_night_5(checked5);
        currentPoint.setIs_night_6(checked6);
        currentPoint.setIs_night_7(checked7);
        currentPoint.setIs_night_8(checked8);
        currentPoint.setIs_night_9(checked9);
        currentPoint.setIs_night_10(checked10);
        currentPoint.setIs_night_11(checked11);
        currentPoint.setIs_night_12(checked12);

        currentPoint.setPort_value_1(String.valueOf(progress1));
        currentPoint.setPort_value_2(String.valueOf(progress2));
        currentPoint.setPort_value_3(String.valueOf(progress3));
        currentPoint.setPort_value_4(String.valueOf(progress4));
        currentPoint.setPort_value_5(String.valueOf(progress5));
        currentPoint.setPort_value_6(String.valueOf(progress6));
        currentPoint.setPort_value_7(String.valueOf(progress7));
        currentPoint.setPort_value_8(String.valueOf(progress8));
        currentPoint.setPort_value_9(String.valueOf(progress9));
        currentPoint.setPort_value_10(String.valueOf(progress10));
        currentPoint.setPort_value_11(String.valueOf(progress11));
        currentPoint.setPort_value_12(String.valueOf(progress12));

        int sum = getSum(progress1,
                progress2,
                progress3,
                progress4,
                progress5,
                progress6,
                progress7,
                progress8,
                progress9,
                progress10,
                progress11,
                progress12);
        currentPoint.setPort_value_general(sum);
        new Handler().postDelayed(() -> {
            DbHelperImpl.getInstance().editTimePointModel(currentPoint);
        }, 100);
        new Handler().postDelayed(() -> {
            getDataFromDb();
            setCurrentTime();
            thisActivity.bleHelper.doSendPointToController(currentPoint);
        }, 700);
    }

    private int getProgressInNight(boolean[] checks, int[] progresses) {
        int res = 0;
        int count = 0;
        for (int i = 0; i < checks.length; i++) {
            if (checks[i]) {
                ++count;
                res = res + progresses[i];
            }
        }
        if (count == 0) {
            return res;
        }
        res = res / count + 128;
        return res;
    }

    private void setNightProgress(boolean checked1, SeekBar seekBar) {
        setValueDefault();
    }

    private void showAddNewPoint() {
        int size = pointModels.size();
        if (size > 24) {
            Toast.makeText(getActivity(), R.string.max_count_points, Toast.LENGTH_SHORT).show();
            return;
        }
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = new SetValueDialog(getActivity(), (selectedHour, selectedMinute, model) -> {
            showProgress(true);
            new Handler().postDelayed(() -> {
                DbHelperImpl.getInstance().addNewPoint(selectedHour, selectedMinute, getMaxId(), model);
            }, 100);
            new Handler().postDelayed(() -> {
                getDataFromDb();
                setCurrentPoint(selectedHour, selectedMinute);
                setCurrentTime();
                setVisibleGoneViews();
            }, 500);
        });
        dialog.show();
    }

    private void setCurrentPoint(int selectedHour, int selectedMinute) {
        if (pointModels != null) {
            String time = refactorTime(selectedHour, selectedMinute);
            for (int i = 0; i < pointModels.size(); i++) {
                TimePointModel point = pointModels.get(i);
                if (point.getTime().equalsIgnoreCase(time)) {
                    currentPoint = point;
                    currentInt = i + 1;
                    return;
                }
            }
        }
    }

    private void showDialogAreYouSure() {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = new AreYouSureDialog(getActivity(), () -> {
            showProgress(true);
            new Handler().postDelayed(() -> {
                DbHelperImpl.getInstance().removePoint(currentPoint);
            }, 100);
            new Handler().postDelayed(() -> {
                getDataFromDb();

                if (pointModels.size() < currentInt) {
                    currentInt = pointModels.size() - 1;
                } else if (pointModels.size() <= 0) {
                    currentInt = 1;
                    currentPoint = null;
                    return;
                } else {
                    currentInt = currentInt - 1;
                }
                Log.d("POINT", "currentPoint currentInt 0000 " + currentInt);
                Log.d("POINT", "currentPoint 11111 " + currentPoint.getTime());
                if (pointModels.size() == 0) {
                    currentPoint = null;
                } else {
                    currentPoint = pointModels.get(currentInt <= 0 ? 0 : currentInt - 1);
                }
                Log.d("POINT", "currentPoint 22222 " + currentPoint.getTime());

                setCurrentTime();
            }, 500);
        });
        dialog.show();
    }

    private long getMaxId() {
        long ida = 0;
        for (TimePointModel model : pointModels) {
            if (ida < model.getId()) {
                ida = model.getId();
            }
        }
        return ida + 1;
    }

    private void setTextView(View view, int text, boolean isChecked) {
        String s = text + "%";
        if (isChecked) {
            float textFloat = text / 10f;
            s = textFloat + "%";
        }
        if (view instanceof CheckBox) {
            ((CheckBox) view).setText(s);
        } else if (view instanceof TextView) {
            ((TextView) view).setText(s);
        }
    }

    public void showProgress(boolean show) {
        thisActivity.runOnUiThread(() -> {
            Log.d("PROGRESS", "Show " + show);
            if (show) {
                progressDialog.show();
            } else {
                progressDialog.dismiss();
            }
        });
    }

    public void updateData() {
    }

    public void updateDataFromDb() {
        getDataFromDb();
        setCurrentTime();
    }
    //    =============================================================================================

    public void updateTime() {
        if (!tv_time_picker.getText().toString().trim().equalsIgnoreCase("--")) {
            tv_description_char.post(() -> {
                tv_description_char.setText(getCueentTime());
            });
        }
    }
}

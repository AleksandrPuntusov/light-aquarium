package com.aleksandrp.lightaquarium.ui.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.aleksandrp.lightaquarium.R;
import com.aleksandrp.lightaquarium.ble.BtListAdapter;
import com.aleksandrp.lightaquarium.db.TimePointModel;
import com.aleksandrp.lightaquarium.ui.fragment.HomeFragment;
import com.aleksandrp.lightaquarium.ui.fragment.SettingsFragment;
import com.jjoe64.graphview.series.LineGraphSeries;

import butterknife.BindView;
import butterknife.OnClick;

import static com.aleksandrp.lightaquarium.utils.Constants.BT_BOUNDED;
import static com.aleksandrp.lightaquarium.utils.Constants.BT_SEARCH;
import static com.aleksandrp.lightaquarium.utils.Constants.REQUEST_CODE_LOC;
import static com.aleksandrp.lightaquarium.utils.Constants.REQ_ENABLE_BT;
import static com.aleksandrp.lightaquarium.utils.Constants.RESULT_SETTINGS;

@SuppressLint("RestrictedApi")
public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = MainActivity.class.getName();

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    public FloatingActionButton fab;

    private FragmentManager mFragmentManager;
    private Fragment currentFragment;
    private HomeFragment homeFrgment;
    private SettingsFragment settingsFragment;

    private boolean doubleBackToExitPressedOnce = false;
    private ProgressDialog progressDialog;

    private LineGraphSeries seriesTemp;
    private LineGraphSeries seriesRand;
    private TimePointModel currentPoint = new TimePointModel();
    private int currentInt = 0;
    private int size = 0;
    public boolean makeSettings = false;

    @Override
    public int getResLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void bleACTION_DISCOVERY_STARTED() {
        if (currentFragment instanceof HomeFragment) {
            ((HomeFragment) currentFragment).bleACTION_DISCOVERY_STARTED();
        }
        bleHelper.setListAdapter(BT_SEARCH);
    }

    @Override
    public void bleACTION_DISCOVERY_FINISHED() {
        if (currentFragment instanceof HomeFragment) {
            ((HomeFragment) currentFragment).bleACTION_DISCOVERY_FINISHED();
        }
    }

    @Override
    public void updateTime() {
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (currentFragment instanceof HomeFragment) {
                backPress();
            } else {
                setHomeFragment();
            }
        }
    }

    @Override
    protected void onStart() {
        bleHelper.setListenerBLE(this);
        super.onStart();
    }

    private void backPress() {
        if (doubleBackToExitPressedOnce) {
            finish();
            System.exit(0);
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(MainActivity.this, "Для выхода из приложения нажмите еще раз",
                Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_settings_light) {
//            if (fab.getVisibility() == View.GONE
            if (!makeSettings
                    && !(currentFragment instanceof SettingsFragment)) {
                Toast.makeText(this, "Включите блютуз для возможности настроек!", Toast.LENGTH_SHORT).show();
            } else {
                setSettingsFragment();
            }
        } else if (id == R.id.nav_settings_ble) {

        } else if (id == R.id.nav_about) {
            startActivity(new Intent(this, AboutUsActivity.class));
        } else if (id == R.id.nav_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
//                        "Hey check out my app at: https://play.google.com/store/apps/details?id=com.google.android.apps.plus");
                    "Раскажите своим друзьям и знакомых о нас");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //    =============================================================================================
    @Override
    public void initUi() {
        setSupportActionBar(toolbar);
        mFragmentManager = getSupportFragmentManager();
        setHomeFragment();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        seriesTemp = new LineGraphSeries();
        seriesRand = new LineGraphSeries();
        seriesTemp.setColor(Color.GREEN);
        seriesRand.setColor(Color.RED);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(getString(R.string.connecting));
        progressDialog.setMessage(getString(R.string.please_wait));

    }

    private void setHomeFragment() {
        if (bleHelper.connectedThread != null) {
////              так захотел Дмитрий  [09.07.19 12:14]
////            В основном окне:
////            2. Место под графиком нужно освободить, там будет отображаться информация о статусе светильника.
////            3. Переключение между рабочими точками в этом окне не нужно. А ускоренный просмотр можно перенести в подпункт меню настроек
//            fab.setVisibility(View.VISIBLE);
            makeSettings = true;
        }
        if (homeFrgment == null) {
            homeFrgment = new HomeFragment();
        }
        setCurrentFragment(homeFrgment);
    }

    private void setSettingsFragment() {
        fab.setVisibility(View.GONE);
        makeSettings = false;
//        if (settingsFragment == null) {
        settingsFragment = new SettingsFragment();
//        }
        setCurrentFragment(settingsFragment);
    }

    private void setCurrentFragment(Fragment currentFragment) {
        this.currentFragment = currentFragment;
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, currentFragment, currentFragment.getClass().getName())
                .commit();
    }

    @Override
    public void showFrameControls() {
        fab.setVisibility(View.GONE);
        makeSettings = false;
        if (currentFragment instanceof HomeFragment) {
            ((HomeFragment) currentFragment).showFrameControls();
        }
    }

    @Override
    public void showFrameLedControls() {
////              так захотел Дмитрий  [09.07.19 12:14]
////            В основном окне:
////            2. Место под графиком нужно освободить, там будет отображаться информация о статусе светильника.
////            3. Переключение между рабочими точками в этом окне не нужно. А ускоренный просмотр можно перенести в подпункт меню настроек
//            fab.setVisibility(View.VISIBLE);
        makeSettings = true;
        if (currentFragment instanceof HomeFragment) {
            ((HomeFragment) currentFragment).showFrameLedControls();
        }
    }

    @Override
    public void setChecked(boolean b) {
        if (currentFragment instanceof HomeFragment) {
            ((HomeFragment) currentFragment).setChecked(b);
        }
    }

    @Override
    public void updateData() {
        if (currentFragment instanceof HomeFragment) {
            ((HomeFragment) currentFragment).updateData();
        }
        if (currentFragment instanceof SettingsFragment) {
            ((SettingsFragment) currentFragment).updateData();
        }
    }

    @Override
    public void updateFromDb() {
        if (currentFragment instanceof SettingsFragment) {
            ((SettingsFragment) currentFragment).updateDataFromDb();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_LOC:
                if (grantResults.length > 0) {
                    for (int gr : grantResults) {
                        // Check if request is granted or not
                        if (gr != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }
                }
                break;
            default:
                return;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RESULT_SETTINGS) {
//            onStart();
        }
        if (requestCode == REQ_ENABLE_BT) {
            if (resultCode == RESULT_OK && bleHelper.isEnabledBluetoothAdapter()) {
                showFrameControls();
                bleHelper.setListAdapter(BT_BOUNDED);
            } else if (resultCode == RESULT_CANCELED) {
                bleHelper.enableBt(true);
            }
        }
        if (currentFragment instanceof HomeFragment) {
            ((HomeFragment) currentFragment).setChart();
        }
    }

    @Override
    public void showProgress(boolean show) {
        if (show) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
        if (currentFragment instanceof SettingsFragment) {
            ((SettingsFragment) currentFragment).showProgress(show);
        }
    }

    @Override
    public void setAdapterListView(BtListAdapter listAdapter) {
        if (currentFragment instanceof HomeFragment) {
            ((HomeFragment) currentFragment).setAdapterListView(listAdapter);
        }
    }

    @OnClick(R.id.fab)
    public void cl() {
        bleHelper.showProgram(fab);
    }


//    @OnLongClick(R.id.fab)
//    public boolean loadPointsInController() {
//        bleHelper.sentProgramsToController();
//        return true;
//    }

}

package com.aleksandrp.lightaquarium.ui.fragment;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.aleksandrp.lightaquarium.R;
import com.aleksandrp.lightaquarium.ble.BtListAdapter;
import com.aleksandrp.lightaquarium.db.DbHelperImpl;
import com.aleksandrp.lightaquarium.db.TimePointModel;
import com.aleksandrp.lightaquarium.ui.activity.MainActivity;
import com.aleksandrp.lightaquarium.utils.SettingsApp;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.aleksandrp.lightaquarium.shar.CharUtils.getDateDouble;
import static com.aleksandrp.lightaquarium.shar.CharUtils.initChars;
import static com.aleksandrp.lightaquarium.shar.CharUtils.setData;
import static com.aleksandrp.lightaquarium.shar.CharUtils.setPointListener;
import static com.aleksandrp.lightaquarium.shar.CharUtils.showVerticalLineTimeChar;
import static com.aleksandrp.lightaquarium.utils.Constants.REQUEST_CODE_LOC;
import static com.aleksandrp.lightaquarium.utils.DateUtils.getCueentTime;

@SuppressLint("RestrictedApi")
public class HomeFragment extends BaseFragment
        implements CompoundButton.OnCheckedChangeListener,
        SeekBar.OnSeekBarChangeListener,
        AdapterView.OnItemClickListener {

    public static final String TAG = MainActivity.class.getName();

    @BindView(R.id.seek_bar_speed)
    SeekBar seek_bar_speed;
    @BindView(R.id.chart_time)
    GraphView chart_time;
    @BindView(R.id.tv_rigt_time)
    TextView tv_rigt_time;
    @BindView(R.id.tv_left_time)
    TextView tv_left_time;
    @BindView(R.id.tv_show_time)
    TextView tv_show_time;
    @BindView(R.id.tv_progress)
    TextView tv_progress;
    @BindView(R.id.tv_description_char)
    TextView tv_description_char;
    @BindView(R.id.switch_enable_bt)
    Switch switchEnableBt;

    @BindView(R.id.frame_control)
    LinearLayout frame_control;
    @BindView(R.id.btn_enable_search)
    Button btnEnableSearch;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    @BindView(R.id.frameLedControls)
    RelativeLayout frameLedControls;
    @BindView(R.id.btn_disconnect)
    Button btnDisconnect;
    @BindView(R.id.gv_graph)
    GraphView gvGraph;
    @BindView(R.id.lv_bt_device)
    ListView listBtDevices;

    private MainActivity thisActivity;
    private LineGraphSeries seriesTemp;
    private LineGraphSeries seriesRand;
    private ArrayList<TimePointModel> pointModels = new ArrayList<>();
    private TimePointModel currentPoint = new TimePointModel();
    private int currentInt = 0;
    private int size = 0;
    private int parseInt = 0;


    @Override
    protected void showTime() {
        updateTime();
    }

    @Override
    protected int getResView() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initUi(View view) {
        thisActivity = (MainActivity) getActivity();

        switchEnableBt.setOnCheckedChangeListener(this);
        seek_bar_speed.setOnSeekBarChangeListener(this);
        listBtDevices.setOnItemClickListener(this);
        setChart();

        seriesTemp = new LineGraphSeries();
        seriesRand = new LineGraphSeries();
        seriesTemp.setColor(Color.GREEN);
        seriesRand.setColor(Color.RED);
        gvGraph.addSeries(seriesTemp);
        gvGraph.addSeries(seriesRand);
        gvGraph.getViewport().setMinX(0);
        gvGraph.getViewport().setMaxX(100);
        gvGraph.getViewport().setXAxisBoundsManual(true);

        seek_bar_speed.setProgress(SettingsApp.getInstance().getStep());
        setStepText();
    }

    public void bleACTION_DISCOVERY_STARTED() {
        btnEnableSearch.setText(R.string.stop_search);
        pbProgress.setVisibility(View.VISIBLE);
    }

    public void bleACTION_DISCOVERY_FINISHED() {
        btnEnableSearch.setText(R.string.start_search);
        pbProgress.setVisibility(View.GONE);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        SettingsApp.getInstance().setStep(progress);
        setStepText();
        chart_time.invalidate();
    }

    public void setStepText() {
        tv_progress.setText("x " + SettingsApp.getInstance().getStep());
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @OnClick({
            R.id.tv_rigt_time,
            R.id.tv_left_time,
            R.id.btn_disconnect,
            R.id.btn_enable_search})
    public void onClickViews(View v) {
        size = pointModels.size() - 1;
        parseInt = currentInt;
        switch (v.getId()) {
            case R.id.tv_rigt_time:
                if (size > 0) {
                    if (parseInt >= size) {
                        parseInt = size;
                    } else if (parseInt >= 24) {
                        parseInt = 24;
                    } else {
                        ++parseInt;
                    }
                    setHoursMinutes(pointModels.get(parseInt - 1));
                }
                break;
            case R.id.tv_left_time:
                if (size > 0) {
                    if (parseInt <= 0) {
                        parseInt = 0;
                    } else {
                        --parseInt;
                    }
                    setHoursMinutes(pointModels.get(parseInt));
                }
                break;
            case R.id.btn_enable_search:
                thisActivity.bleHelper.enableSearch();
                break;
            case R.id.btn_disconnect:
                thisActivity.bleHelper.pause();
                thisActivity.bleHelper.disconnectTreads();
                showFrameControls();
                break;
        }
        currentInt = parseInt;
        if (pointModels.size() == 0) {
            return;
        }
        tv_show_time.setText(pointModels.get(currentInt).getTime());
    }

    private void setHoursMinutes(TimePointModel currentPoint) {
        this.currentPoint = currentPoint;
        tv_show_time.setText(currentPoint.getTime());
    }

    //    =============================================================================================

    public void updateTime() {
        tv_description_char.setText(getCueentTime());
    }

    //    =============================================================================================

    public void setChart() {
        initChars(getActivity(), chart_time);
        setPointListener(x -> {
            for (int i = 0; i < pointModels.size(); i++) {
                TimePointModel model = pointModels.get(i);
                if (getDateDouble(model.getTime()) == x) {
                    currentPoint = model;
                }
            }
        });
    }

    public void showFrameMessage() {
        thisActivity.bleHelper.destroy();
        thisActivity.makeSettings = true;
        frameLedControls.setVisibility(View.GONE);
        frame_control.setVisibility(View.GONE);
    }

    public void showFrameControls() {
        frameLedControls.setVisibility(View.GONE);
        frame_control.setVisibility(View.VISIBLE);
    }

    public void showFrameLedControls() {
        frame_control.setVisibility(View.GONE);
    }

    public void setChecked(boolean b) {
        switchEnableBt.setChecked(b);
    }

    public void updateData() {
        pointModels = DbHelperImpl.getInstance().readAllPoints();
        updateDataInChart(pointModels);
        size = pointModels.size();
        if (size > 0 && currentInt <= size) {
            currentPoint = pointModels.get(currentInt);
            tv_show_time.setText(currentPoint.getTime());
        } else {
            tv_show_time.setText("");
        }

        showVerticalLineTimeChar(getCueentTime());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_LOC:
                if (grantResults.length > 0) {
                    for (int gr : grantResults) {
                        // Check if request is granted or not
                        if (gr != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }
                }
                break;
            default:
                return;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.equals(switchEnableBt)) {
            thisActivity.bleHelper.enableBt(isChecked);
            if (!isChecked) {
                showFrameMessage();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent.equals(listBtDevices)) {
            thisActivity.bleHelper.makeConnect(position);
        }
    }

    public void setAdapterListView(BtListAdapter listAdapter) {
        listBtDevices.setAdapter(listAdapter);
    }

    private void updateDataInChart(ArrayList<TimePointModel> pointModels) {
        Log.d("IST_POINTS", "IST_POINTS " + pointModels.toString());
        // add data
        setData(pointModels);
    }
}

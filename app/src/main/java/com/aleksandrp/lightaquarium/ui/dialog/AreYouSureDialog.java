package com.aleksandrp.lightaquarium.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.aleksandrp.lightaquarium.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AreYouSureDialog extends AlertDialog {

    private Context mContext;
    private AreYouSureDialogListener listener;

    public AreYouSureDialog(Context mContext, AreYouSureDialogListener listener) {
        super(mContext);
        this.mContext = mContext;
        this.listener = listener;

        // Init dialog view
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_are_you_sure, null);
        ButterKnife.bind(this, view);

        this.setView(view);
    }

    @OnClick(R.id.tv_title_sure_yes)
    public void clickOk() {
        if (listener != null) {
            listener.clickOk();
        }
        cancel();
    }
    @OnClick(R.id.tv_title_sure_no)
    public void clickCancel() {
        cancel();
    }

    public interface AreYouSureDialogListener {
        void clickOk();
    }
}

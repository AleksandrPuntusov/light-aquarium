package com.aleksandrp.lightaquarium.ble;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.util.Log;
import android.widget.Toast;

import com.aleksandrp.lightaquarium.BleListener;
import com.aleksandrp.lightaquarium.R;
import com.aleksandrp.lightaquarium.db.DbHelperImpl;
import com.aleksandrp.lightaquarium.db.TimePointModel;
import com.aleksandrp.lightaquarium.utils.Comands;
import com.aleksandrp.lightaquarium.utils.SettingsApp;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.aleksandrp.lightaquarium.utils.Comands.ComandsEnum.CHECK_TIME_COMMAND;
import static com.aleksandrp.lightaquarium.utils.Comands.ComandsEnum.GET_PROGRAMS_COMMAND;
import static com.aleksandrp.lightaquarium.utils.Comands.ComandsEnum.SEND_POINT_FOR_SHOW;
import static com.aleksandrp.lightaquarium.utils.Comands.ComandsEnum.SEND_PROGRAM_FOR_SHOW;
import static com.aleksandrp.lightaquarium.utils.Comands.ComandsEnum.SEND_PROGRAM_TO_CONTROLLER;
import static com.aleksandrp.lightaquarium.utils.Comands.ComandsEnum.START_COMMAND;
import static com.aleksandrp.lightaquarium.utils.Comands.FIRST_COMMAND;
import static com.aleksandrp.lightaquarium.utils.Comands.IDENTFICATOR_CHECK_TIME_COMMAND;
import static com.aleksandrp.lightaquarium.utils.Comands.IDENTFICATOR_CONFIG_CONTROLER_00;
import static com.aleksandrp.lightaquarium.utils.Comands.IDENTFICATOR_POWER_20;
import static com.aleksandrp.lightaquarium.utils.Comands.IDENTFICATOR_REQUEST_PROGRAMM_21;
import static com.aleksandrp.lightaquarium.utils.Comands.IDENTFICATOR_SEND_DATE_22;
import static com.aleksandrp.lightaquarium.utils.Comands.TIME_COMMAND;
import static com.aleksandrp.lightaquarium.utils.Comands.buildGetPrograms;
import static com.aleksandrp.lightaquarium.utils.Comands.buildPointProgramForShow;
import static com.aleksandrp.lightaquarium.utils.Comands.buildPointProgramForShowString;
import static com.aleksandrp.lightaquarium.utils.Comands.buildPointPrograms;
import static com.aleksandrp.lightaquarium.utils.Constants.BT_BOUNDED;
import static com.aleksandrp.lightaquarium.utils.Constants.BT_SEARCH;
import static com.aleksandrp.lightaquarium.utils.Constants.DELAY_TIMER;
import static com.aleksandrp.lightaquarium.utils.Constants.REQUEST_CODE_LOC;
import static com.aleksandrp.lightaquarium.utils.Constants.REQ_ENABLE_BT;

public class BleHelper {

    public static final String TAG = BleHelper.class.getName();

    private BluetoothAdapter bluetoothAdapter;
    private BtListAdapter listAdapter;
    private ArrayList<BluetoothDevice> bluetoothDevices = new ArrayList<>();

    public ConnectedThread connectedThread;
    private ConnectThread connectThread;
    private Handler handler;
    private Runnable timer;
    private Handler handlerStep;
    private Runnable timerStep;
    private Handler handlerStepWorker;
    private Runnable timerStepWorker;

    private BleListener listener;
    private Activity activity;
    private BleHelper bleHelper;

    public BleHelper(Activity activity, BleListener listener) {
        if (bleHelper == null) {
            bleHelper = new BleHelper();
        }
        this.activity = activity;
        setListenerBLE(listener);
    }

    private BleHelper() {
    }

    public void setListenerBLE(BleListener listener) {
        this.listener = listener;
    }

    public void pause() {
        cancelTimer();
    }

    public void resume() {
        if (connectedThread != null) {
            startTimer();
        }
    }

    private void cancelTimer() {
        if (handler != null) {
            handler.removeCallbacks(timer);
            handler = null;
            timer = null;
        }
    }

    private void cancelTimerStep() {
        if (handlerStep != null) {
            handlerStep.removeCallbacks(timerStep);
            handlerStep = null;
            timerStep = null;
        }
    }

    private void stopWorkerThread() {
        if (handlerStepWorker != null) {
            handlerStepWorker.removeCallbacks(timerStepWorker);
            handlerStepWorker = null;
            timerStepWorker = null;
        }
    }

    public void destroy() {
        cancelTimer();
        disconnectTreads();
    }

    public void disconnectTreads() {
        if (connectedThread != null) {
            connectedThread.cancel();
            connectedThread = null;
        }
        if (connectThread != null) {
            connectThread.cancel();
            connectThread = null;
        }
    }

    public boolean isEnabledBluetoothAdapter() {
        return bluetoothAdapter.isEnabled();
    }

    private void startTimer() {
        if (handler != null && timer != null) {
            return;
        }
        handler = new Handler();
        handler.postDelayed(timer = new Runnable() {
            @Override
            public void run() {
                if (listener != null && connectedThread != null) {
                    listener.updateDataBle();
                }
                handler.postDelayed(this, DELAY_TIMER);
            }
        }, DELAY_TIMER);
    }

    /**
     * check bluetooth on device
     */
    public void checkBluetooth() {
        if (listener != null) {
            listener.registerReceiver();
        }
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(activity, R.string.bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onCreate: " + activity.getString(R.string.bluetooth_not_supported));
            activity.finish();
        }
        if (this.bluetoothAdapter != null) {
            return;
        }
        this.bluetoothAdapter = bluetoothAdapter;
        if (this.bluetoothAdapter.isEnabled()) {
            if (listener != null) {
                listener.showFrameControls();
                listener.setChecked(true);
            }
            setListAdapter(BT_BOUNDED);
        }
    }

    public void showProgress(boolean show) {
        if (listener != null) {
            listener.showProgress(show);
        }
    }

    public void makeConnect(int position) {
        BluetoothDevice device = bluetoothDevices.get(position);
        if (device != null) {
            connectThread = new ConnectThread(device);
            connectThread.start();
            startTimer();
        }
    }

    public void addDevice(BluetoothDevice device) {
        if (device != null) {
            if (!bluetoothDevices.contains(device)) {
                bluetoothDevices.add(device);
            }
            listAdapter.notifyDataSetChanged();
        }
    }

    public void enableBt(boolean flag) {
        if (flag) {
            if (connectThread != null) {
                return;
            }
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(intent, REQ_ENABLE_BT);
        } else {
            bluetoothAdapter.disable();
        }
    }

    public void setListAdapter(int type) {
        bluetoothDevices.clear();
        int iconType = R.drawable.ic_bluetooth_bounded_device;
        switch (type) {
            case BT_BOUNDED:
                bluetoothDevices = getBoundedBtDevices();
                iconType = R.drawable.ic_bluetooth_bounded_device;
                break;
            case BT_SEARCH:
                iconType = R.drawable.ic_bluetooth_search_device;
                break;
        }
        listAdapter = new BtListAdapter(activity, bluetoothDevices, iconType);
        if (listener != null) {
            listener.setAdapterListView(listAdapter);
        }
    }


    private ArrayList<BluetoothDevice> getBoundedBtDevices() {
        Set<BluetoothDevice> deviceSet = bluetoothAdapter.getBondedDevices();
        ArrayList<BluetoothDevice> tmpArrayList = new ArrayList<>();
        if (deviceSet.size() > 0) {
            for (BluetoothDevice device : deviceSet) {
                if (!tmpArrayList.contains(device)) {
                    tmpArrayList.add(device);
                }
            }
        }
        return tmpArrayList;
    }

    public void enableSearch() {
        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        } else {
            accessLocationPermission();
            bluetoothAdapter.startDiscovery();
        }
    }

    /**
     * Запрос на разрешение данных о местоположении (для Marshmallow 6.0 и выше)
     */
    private void accessLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int accessCoarseLocation = activity.checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION);
            int accessFineLocation = activity.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);

            List<String> listRequestPermission = new ArrayList<String>();
            if (accessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
                listRequestPermission.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (accessFineLocation != PackageManager.PERMISSION_GRANTED) {
                listRequestPermission.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (!listRequestPermission.isEmpty()) {
                String[] strRequestPermission = listRequestPermission.toArray(new String[listRequestPermission.size()]);
                activity.requestPermissions(strRequestPermission, REQUEST_CODE_LOC);
            }
        }
    }

    public void startWrite(Comands.ComandsEnum startComand, Object... arg) {
        TimePointModel model = null;
        StringBuilder builder = new StringBuilder();
        switch (startComand) {
            case START_COMMAND:
                builder.append(FIRST_COMMAND);
                break;
            case CHECK_TIME_COMMAND:
                builder.append(TIME_COMMAND);
                break;
            case SEND_PROGRAM_TO_CONTROLLER:
                model = ((TimePointModel) arg[0]);
                builder.append(buildPointPrograms(model, ((int) arg[1]), ((int) arg[2])));
                break;
            case SEND_POINT_FOR_SHOW:
                model = ((TimePointModel) arg[0]);
                builder.append(buildPointProgramForShow(model));
                break;
            case SEND_PROGRAM_FOR_SHOW:
                String time = ((String) arg[0]);
                builder.append(buildPointProgramForShowString(time));
                break;
            case GET_PROGRAMS_COMMAND:
                int curentPoint = (int) arg[0];
                builder.append(buildGetPrograms(curentPoint));
                break;
        }
        String command = builder.toString();
        Log.d("BLE_TAG", "command builder " + startComand + " __ " + builder.toString());
        if (connectedThread != null) {
            connectedThread.write(command);
        }
    }

//=======================================================================================

    private class ConnectedThread extends Thread {
        private final InputStream inputStream;
        private final OutputStream outputStream;
        private boolean isConnected = false;

        public ConnectedThread(BluetoothSocket bluetoothSocket) {
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                inputStream = bluetoothSocket.getInputStream();
                outputStream = bluetoothSocket.getOutputStream();
                isConnected = true;
            } catch (Exception e) {
                isConnected = false;
                e.printStackTrace();
                Log.d("BLE_TAG", "IOException 111  " + e.getMessage());
            }
            this.inputStream = inputStream;
            this.outputStream = outputStream;
        }

        @Override
        public void run() {
            BufferedInputStream bis = new BufferedInputStream(inputStream);
            ArrayList<Integer> params = new ArrayList<>();
            while (isConnected) {
                try {
                    int bytes = bis.read();
                    params.add(bytes);
                    if (params.size() == 20) {
                        saveInDB(params);
                        params.clear();
                    }
                } catch (IOException io) {
                    Log.d("BLE_TAG_read", "IOException 2323 " + io.getMessage());
                    io.printStackTrace();
                    params.clear();
                } catch (Exception e) {
                    Log.d("BLE_TAG_read", "IOException 2323 " + e.getMessage());
                    e.printStackTrace();
                    params.clear();
                }
            }

            try {
                bis.close();
                cancel();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void write(String command) {
            byte[] bytes20 = new byte[20];
            String[] split = command.split(" ");
            for (int i = 0; i < split.length; i++) {
                String substring = split[i];
                Log.d("BYTE___", "substring " + substring);
                Integer parseInt = Integer.parseInt(substring);
                Log.d("BYTE___", "parseInt " + parseInt);
                byte b = parseInt.byteValue();
                Log.d("BYTE___", "b " + b);
                bytes20[i] = b;
            }

            if (outputStream != null) {
                try {
                    outputStream.write(bytes20);
                    Log.d("BLE_TAG", "write bytes" + bytes20.toString());
                    outputStream.flush();
                } catch (IOException e) {
                    Log.d("BLE_TAG", "write bytes IOException 3333 " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        public void cancel() {
            try {
                isConnected = false;
                inputStream.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private class ConnectThread extends Thread {
        private BluetoothSocket bluetoothSocket = null;
        private boolean success = false;

        public ConnectThread(BluetoothDevice device) {
            try {
                Method method = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
                bluetoothSocket = (BluetoothSocket) method.invoke(device, 1);
                showProgress(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            try {
                bluetoothSocket.connect();
                success = true;
                showProgress(false);
            } catch (IOException e) {
                e.printStackTrace();
                success = false;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showProgress(false);
                        Toast.makeText(activity, "Не могу соединиться!", Toast.LENGTH_SHORT).show();
                    }
                });
                cancel();
            }

            if (success) {
                connectedThread = new ConnectedThread(bluetoothSocket);
                connectedThread.start();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (listener != null) {
                            startWrite(CHECK_TIME_COMMAND);
                            listener.showFrameLedControls();
                        }
                    }
                });
            }
        }

        public boolean isConnect() {
            return bluetoothSocket.isConnected();
        }

        public void cancel() {
            try {
                Log.d(TAG, "cancel: " + this.getClass().getSimpleName());
                bluetoothSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

//==========================================================================================

    private void saveInDB(ArrayList<Integer> params) {
        Log.d("BLE_TAG_read___", "lastSensorValues__read_______ " + params.toString());

        int config = params.get(0);
        if (config != 129) {
            return;
        }
        int identificator = params.get(1);
        int countData = params.get(2);
        int count_pointsd = params.get(3);
        int data_1 = params.get(4);
        int data_2 = params.get(5);
        int data_3 = params.get(6);
        int data_4 = params.get(7);
        int data_5 = params.get(8);
        int data_6 = params.get(9);
        int data_7 = params.get(10);
        int data_8 = params.get(11);
        int data_9 = params.get(12);
        int data_10 = params.get(13);
        int data_11 = params.get(14);
        int data_12 = params.get(15);
        int data_13 = params.get(16);
        int data_14 = params.get(17);
        int data_15 = params.get(18);
        int summ = params.get(19);

        Log.d("BLE_TAG_read___", "config config ::: " + config);
        Log.d("BLE_TAG_read___", "config identificator ::: " + identificator);
        Log.d("BLE_TAG_read___", "config countData ::: " + countData);
        Log.d("BLE_TAG_read___", "config data_1 ::: " + data_1);
        Log.d("BLE_TAG_read___", "config data_2 ::: " + data_2);
        Log.d("BLE_TAG_read___", "config data_3 ::: " + data_3);
        Log.d("BLE_TAG_read___", "config data_4 ::: " + data_4);
        Log.d("BLE_TAG_read___", "config data_5 ::: " + data_5);
        Log.d("BLE_TAG_read___", "config data_6 ::: " + data_6);
        Log.d("BLE_TAG_read___", "config data_7 ::: " + data_7);
        Log.d("BLE_TAG_read___", "config data_8 ::: " + data_8);
        Log.d("BLE_TAG_read___", "config data_9 ::: " + data_9);
        Log.d("BLE_TAG_read___", "config data_10 ::: " + data_10);
        Log.d("BLE_TAG_read___", "config data_11 ::: " + data_11);
        Log.d("BLE_TAG_read___", "config data_12 ::: " + data_12);
        Log.d("BLE_TAG_read___", "config data_13 ::: " + data_13);
        Log.d("BLE_TAG_read___", "config data_14 ::: " + data_14);
        Log.d("BLE_TAG_read___", "config data_15 ::: " + data_15);
        Log.d("BLE_TAG_read___", "config summ ::: " + summ);

        if (countData == 0 || countData == 100) {
            if (identificator == IDENTFICATOR_CHECK_TIME_COMMAND) {
                startWrite(START_COMMAND);
            }
            return;
        }
        switch (identificator) {
//            case IDENTFICATOR_CHECK_TIME_COMMAND:
//                startWrite(START_COMMAND);
//                break;
            case IDENTFICATOR_CONFIG_CONTROLER_00:
                DbHelperImpl.getInstance().saveController(
                        identificator,
                        countData,
                        count_pointsd,
                        data_1,
                        data_2,
                        data_3,
                        data_4,
                        data_5,
                        data_6,
                        data_7,
                        data_8,
                        data_9,
                        data_10,
                        data_11,
                        data_12,
                        data_13,
                        data_14,
                        data_15);
                SettingsApp.getInstance().setCanelController(count_pointsd);
                SettingsApp.getInstance().setPoints(data_4);
                DbHelperImpl.getInstance().removeAllPoints();
                getProgramsFromController();
                break;
            case IDENTFICATOR_POWER_20:
//                    NO DO
                break;
            case IDENTFICATOR_REQUEST_PROGRAMM_21:
//                    NO DO
                break;
            case IDENTFICATOR_SEND_DATE_22:
//                    NO DO
                break;
            default:
                activity.runOnUiThread(() -> {
                    DbHelperImpl.getInstance().savePoints(
                            identificator,
                            countData,
                            count_pointsd,
                            data_1,
                            data_2,
                            data_3,
                            data_4,
                            data_5,
                            data_6,
                            data_7,
                            data_8,
                            data_9,
                            data_10,
                            data_11,
                            data_12,
                            data_13,
                            data_14,
                            data_15,
                            new DbHelperImpl.SaveListener() {
                                @Override
                                public void isSave(boolean save) {
                                    if (listener != null) {
                                        listener.updateDataFromDb();
                                    }
                                }
                            });
                });
        }
    }

    public void getProgramsFromController() {
        int points = SettingsApp.getInstance().getPoints();
        if (points > 0) {
            for (int i = 0; i < points; i++) {
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startWrite(GET_PROGRAMS_COMMAND, i + 1);
            }
        }
        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        if (listener != null) {
            listener.updateDataBle();
            listener.showProgress(false);
        }
    }

    public void sentProgramsToController() {
        ArrayList<TimePointModel> pointModels = DbHelperImpl.getInstance().readAllPoints();
        for (int i = 0; i < pointModels.size(); i++) {
            startWrite(SEND_PROGRAM_TO_CONTROLLER, pointModels.get(i), i + 1, pointModels.size());
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        startWrite(START_COMMAND);


        if (listener != null) {
            listener.showProgress(false);
        }
    }

    public void doSendPointToController(TimePointModel model) {
        startWrite(SEND_POINT_FOR_SHOW, model);
    }

    public void showProgram(FloatingActionButton fab) {
        if (handlerStep != null && timerStep != null) {
            enableFab(fab, true);
            cancelTimerStep();
            return;
        }
        enableFab(fab, false);
        ArrayList<TimePointModel> pointModels = DbHelperImpl.getInstance().readAllPoints();
        if (pointModels.size() <= 0) {
            enableFab(fab, true);
            return;
        }
        int stepRequest = 1000;

        long totalIn_24 = 60 * 60 * 24;
        final long[] totalIn_24_count = {0};


        final String[] HH_MM = {getStringTime(totalIn_24_count[0])};
        startWrite(SEND_PROGRAM_FOR_SHOW, HH_MM[0]);
        handlerStep = new Handler();
        handlerStep.postDelayed(timerStep = new Runnable() {
            @Override
            public void run() {
                long timerStepCount = totalIn_24 / ((5 * 60) / SettingsApp.getInstance().getStep());  // 5 minute show
                Log.d("TIME_COUNT", "timerStepCount " + timerStepCount);
                totalIn_24_count[0] = totalIn_24_count[0] + timerStepCount;
                if (totalIn_24_count[0] >= totalIn_24) {
                    HH_MM[0] = getStringTime(totalIn_24 - 60);
                    startWrite(SEND_PROGRAM_FOR_SHOW, HH_MM[0]);
                    cancelTimerStep();
                    enableFab(fab, true);
                    return;
                }
                HH_MM[0] = getStringTime(totalIn_24_count[0]);
                startWrite(SEND_PROGRAM_FOR_SHOW, HH_MM[0]);
                handlerStep.postDelayed(this, stepRequest);
            }
        }, stepRequest);
    }

    private void enableFab(FloatingActionButton fab, boolean b) {
        if (b) {
            fab.setImageResource(android.R.drawable.ic_media_play);
        } else {
            fab.setImageResource(R.drawable.ic_stop_white);
        }
    }

    private String getStringTime(long time) {
        time = (time - 3 * 60 * 60) * 1000;
        if (time == 0) {
            return "00 00 ";
        }
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(time);
        formattedDate = formattedDate.replaceAll(":", " ") + " ";
        Log.d("TIME_COUNT", "formattedDate " + formattedDate);
        return formattedDate;
    }

}
